/**
 * @ngdoc function
 * @name bat11App.controller:AppCtrl
 * @description
 * # MainCtrl
 * Controller of the bat11App
 */

(function () {
    'use strict';
    angular
        .module('bat11App')

            .controller('ApplicationCtrl', ApplicationCtrl)
            .controller('AccordionDemoCtrl', AccordionDemoCtrl)
            .controller('CarouselSliderCtrl', CarouselSliderCtrl)
            .controller('TabController', TabController)
            .controller('breakingNewsCtrl', breakingNewsCtrl)
            .controller('headerMenuCtrl', headerMenuCtrl)
            .controller('paginationCtrl', paginationCtrl)
            .controller('timerCtrl',timerCtrl)
            .filter('startFrom', function() {
                return function(input, start) {
                    start = +start; //parse to int
                    return input.slice(start);
                }
            });

                       
            // create the controller and inject Angular's $scope
            function ApplicationCtrl($scope, $http, $localStorage, $location, $rootScope, $anchorScroll, $timeout, $window, $cookies, $filter, $interval) {
 
				var vm = $scope;
                vm.isIE = isIE();
                vm.isSmart = isSmart();
                // config
                vm.app = {
                    name: 'bat11App',
                    version: '1.0.0',
                    // for chart colors
                    color: {
                        'primary': '#0cc2aa',
                        'accent': '#a88add',
                        'warn': '#fcc100',
                        'info': '#6887ff',
                        'success': '#6cc788',
                        'warning': '#f77a99',
                        'danger': '#f44455',
                        'white': '#ffffff',
                        'light': '#f1f2f3',
                        'dark': '#2e3e4e',
                        'black': '#2a2b3c'
                    },
                    setting: {
                        theme: {
                            primary: 'primary',
                            accent: 'accent',
                            warn: 'warn'
                            // set Theme 
                            //primary: 'danger',
                            //accent: 'grey-100',
                            //warn: 'grey-300'
                        },
                        folded: false,
                        boxed: false,
                        container: false,
                        themeID: 1,
                        bg: ''
                    }
                };
                //   primary:'danger', accent:'grey-100', warn:'grey-300'

                var setting = vm.app.name + '-Setting';
                // save settings to local storage
                if (angular.isDefined($localStorage[setting])) {
                    vm.app.setting = $localStorage[setting];
                } else {
                    $localStorage[setting] = vm.app.setting;
                }
                // watch changes
                $scope.$watch('app.setting', function () {
                    $localStorage[setting] = vm.app.setting;
                }, true);

                getParams('bg') && (vm.app.setting.bg = getParams('bg'));

                vm.setTheme = setTheme;
                setColor();

                function setTheme(theme) {
                    vm.app.setting.theme = theme.theme;
                    setColor();
                    if (theme.url) {
                        $timeout(function () {
                            $window.location.href = theme.url;
                        }, 100, false);
                    }
                };

                function setColor() {
                    vm.app.setting.color = {
                        primary: getColor(vm.app.setting.theme.primary),
                        accent: getColor(vm.app.setting.theme.accent),
                        warn: getColor(vm.app.setting.theme.warn)
                    };
                };

                function getColor(name) {
                    return vm.app.color[name] ? vm.app.color[name] : palette.find(name);
                };

                $rootScope.$on('$stateChangeSuccess', openPage);

                function openPage() {
                    // goto top
                    $location.hash('content');
                    $anchorScroll();
                    $location.hash('');
                    // hide open menu
                    $('#aside').modal('hide');
                    $('body').removeClass('modal-open').find('.modal-backdrop').remove();
                    $('.navbar-toggleable-sm').collapse('hide');
                };

                vm.goBack = function () {
                    $window.history.back();
                };

                function isIE() {
                    return !!navigator.userAgent.match(/MSIE/i) || !!navigator.userAgent.match(/Trident.*rv:11\./);
                }

                function isSmart() {
                    // Adapted from http://www.detectmobilebrowsers.com
                    var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
                    // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
                    return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
                }

                function getParams(name) {
                    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                }

                if($location.host() == 'localhost' || $location.host() == '127.0.0.1' ){
                    $scope.host = 'http://localhost:82';
                }else{
 
                    $scope.host = 'http://35.231.238.182:82';
 
                }                

                //FIRE API CALL TO LARAVEL SYSTEM FOR Footer PAGE
                $http({
                    method: 'POST',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: $scope.host+'/api/page-content/footer'
                }).then(function successCallback(response) {
                    if(response.data.code == '200'){
                        $scope.footerPage = response.data.getPageContentObj;

                    }
                }, function errorCallback(data) {
                    
                });


                //FIRE API CALL TO LARAVEL SYSTEM FOR LATEST NEWS
                if($location.path() == '/app/home' || $location.path() == '/app/news'  ){  
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/latest-news'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.latestNews = response.data.getLatestNewsObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //OPEN FULL NEWS PAGE FOR THAT PARTICULAR NEWS
                $scope.openNews = function(newsId){
                    $scope.onelatestNews = [];
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-news-details/'+newsId
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.onelatestNews = response.data.getNewsDetailsObj;
                            $cookies.put('newsId',response.data.getNewsDetailsObj[0].id);
                            window.location = '/app/index.html?#/app/newsReader';  
                        }
                    }, function errorCallback(data) {
                        
                    });
                }                
                
                if($location.path() == '/app/newsReader' ){  
                    $scope.openNews($cookies.get('newsId'));
                } 

                //FIRE API CALL TO LARAVEL SYSTEM FOR BLOG
                if($location.path() == '/app/home' || $location.path() == '/app/blog'){  
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/all-blogs-data'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.blogData = response.data.getBlogsDetailsObj;
                        }else{

                        }
                    }, function errorCallback(data) {
                        
                    });
                }    

                //OPEN FULL BLOG PAGE FOR THAT PARTICULAR BLOG
                $scope.openBlog = function(blogSlug){
                    $scope.oneBlogData = [];
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-blogs-details/'+blogSlug
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.oneBlogData = response.data.getBlogsDetailsObj;
                            $cookies.put('blogSlug',response.data.getBlogsDetailsObj[0].slug);
                            window.location = '/app/index.html?#/app/blogReader'; 
                        }
                    }, function errorCallback(data) {
                        
                    });
                    
                }      
                
                if($location.path() == '/app/blogReader' ){  
                    $scope.openBlog($cookies.get('blogSlug'));
                } 


                //FIRE API CALL TO LARAVEL SYSTEM FOR CAROUSEL IMAGES
                if($location.path() == '/app/home' || $location.path() == '/app/photos'){  
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/carousel-image'
                        }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $scope.imgData = response.data.getCrousalImageObj;

                            }
                        }, function errorCallback(data) {
                            
                    });
                }        

                //FIRE API CALL TO LARAVEL SYSTEM FOR VIDEOS
                if($location.path() == '/app/home' || $location.path() == '/app/videos'){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/videos-urls'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.videosData = response.data.getYoutubeVideoObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }    
              
                //FIRE API CALL TO LARAVEL SYSTEM FOR UPCOMING MATCHES
                if($location.path() == '/app/home' || $location.path() == '/app/upcoming' ){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-upcoming-matches'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.upcomingMatchesData = response.data.getUpcomingMatchDetailsObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }    

                //FIRE API CALL TO LARAVEL SYSTEM FOR TEAMS
                if($location.path() == '/app/home' || $location.path() == '/app/teams' ){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-teams'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.teamDetailsData = response.data.getTeamsDetailsObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }
                
                //FETCHING THE TEAM DETAILS`            
                $scope.teamDetails = function(teamId){
                    $scope.oneTeamDetails = [];
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-team-details/'+teamId
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.oneTeamDetails = response.data.getPlayerListObj;

                            $cookies.put('teamId',response.data.getPlayerListObj[0].id);
                            window.location = '/app/index.html?#/app/teamDetails';  
                        }
                    }, function errorCallback(data) {
                    });
                }

                if($location.path() == '/app/teamDetails' ){  
                    $scope.teamDetails($cookies.get('teamId'));
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR PLAYERS
                if($location.path() == '/app/home' || $location.path() == '/app/players' ){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-players'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.playerDetailsData = response.data.getPlayerListObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //FETCHING THE PLAYER DETAILS            
                $scope.playerDetails = function(playerKey){
                    $scope.onePlayerDetails = [];
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-players-details/'+playerKey
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.onePlayerDetails = response.data.getTeamsDetailsObj;

                            $scope.objTest  = JSON.parse($scope.onePlayerDetails[0].test);
                            $scope.objOdi   = JSON.parse($scope.onePlayerDetails[0].oneDay);
                            $scope.objT20   = JSON.parse($scope.onePlayerDetails[0].t20);
                            $scope.objIpl   = JSON.parse($scope.onePlayerDetails[0].iplT20);

                            $cookies.put('playerKey',response.data.getTeamsDetailsObj[0].playerKey);
                            window.location = '/app/index.html?#/app/playerDetails';  
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                if($location.path() == '/app/playerDetails' ){  
                    $scope.playerDetails($cookies.get('playerKey'));
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR FETCHING THE ALL LEAGUES
                if($location.path() == '/app/home' || $location.path() == '/app/upcoming' ){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-leaguenames'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objLeagueData = response.data.getLeagueTeamsObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //FETCHING THE Teams            
                $scope.getTeam = function(slug){
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-leaguenames-details/'+slug
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                           $scope.objLeagueTeams = response.data.getLeagueTeamObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                if($location.path() == '/app/teams' ){  
                    $scope.getTeam('icc-team');
                }

                //FIRE API CALL TO LARAVEL SYSTEM
                if($location.path() == '/app/home'){ 
                    $http({

                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/testimonial-content'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.testimonial = response.data.getTestimonialObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }
              
                $scope.registrationDetails  =   function(){

                    var dateToday   = new Date();
                    $scope.minDate  = new Date(dateToday.getFullYear() - 100, dateToday.getMonth(), dateToday.getDate());
                    $scope.yrRange  = new Date(dateToday.getFullYear() - 18, dateToday.getMonth(), dateToday.getDate());

                    $scope.gender   = ["Male","Female"];
                    $scope.day      = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14","15",
                                       "16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"];

                    $scope.month    =   [
                                            {id: "1", name : "Jan"},
                                            {id: "2", name : "Feb"},
                                            {id: "3", name : "Mar"},
                                            {id: "4", name : "Apr"},
                                            {id: "5", name : "May"},
                                            {id: "6", name : "Jun"},
                                            {id: "7", name : "Jul"},
                                            {id: "8", name : "Aug"},
                                            {id: "9", name : "Sep"},
                                            {id: "10", name : "Oct"},
                                            {id: "11", name : "Nov"},
                                            {id: "12", name : "Dec"}
                                        ];

                    $scope.year     = ["1950", "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", 
                                        "1960", "1961","1962","1963","1964","1965","1966","1967","1968","1969","1970",
                                        "1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981",
                                        "1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992",
                                        "1993","1994","1995","1996","1997","1998","1999","2000"];

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-country'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objCountry = response.data.getAllCountryObj;
                        }
                    }, function errorCallback(data) {
                    });
                }
                
                $scope.bankDetails = function(){
                    var id      = $cookies.get('id');
                    var apikey  = $cookies.get('apikey');

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {id: id, apikey: apikey},
                        url: $scope.host+'/api/fetch-bank-details'
                    }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $scope.objBankDetail = response.data.getBankDetailObj;

                                $scope.bankname = $scope.objBankDetail[0].bankname;
                                $scope.branch = $scope.objBankDetail[0].branch;
                                $scope.holdername = $scope.objBankDetail[0].holdername;
                                $scope.accountnumber = $scope.objBankDetail[0].accountnumber;
                                $scope.ifsccode = $scope.objBankDetail[0].ifsccode;
                            }
                        }, function errorCallback(data) {
                    });
                }

                $scope.invitationDetails = function(){
                    var id       = $cookies.get('id');
                    var apikey   = $cookies.get('apikey');

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {id: id,apikey: apikey},
                        url: $scope.host+'/api/fetch-send-request'
                    }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $('.senderrequest').removeClass('hide');
                                $scope.objSendRequestObj = response.data.getSendRequestObj;
                            }else{
                                $('.senderrequest').addClass('hide');
                                $('.errorsenderrequest').html('Your have not send any invitation yet!');
                                $('.errorsenderrequest').removeClass('hide');
                                // $scope.alertMessage();
                            } 
                        }, function errorCallback(data) {
                    });


                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {id: id,apikey: apikey},
                        url: $scope.host+'/api/fetch-recive-request'
                    }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $('.reciverrequest').removeClass('hide');
                                $scope.objRecieveRequestObj = response.data.getReciveRequestObj;
                            }else{
                                $('.reciverrequest').addClass('hide');
                                $('.errorreciverrequest').html('Your have not any invitation yet!');
                                $('.errorreciverrequest').removeClass('hide');
                                // $scope.alertMessage();
                            }
                        }, function errorCallback(data) {
                    });
                }

                $scope.withdrawDetails = function(){
                    var id                  = $cookies.get('id');
                    var apikey              = $cookies.get('apikey');
                    
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {id: id,apikey: apikey},
                        url: $scope.host+'/api/user-verified-documents'
                    }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $('.verifiedDetails').removeClass('hide');
                                $scope.objUserProof = response.data.objUserProof;
                                $scope.notverified = parseInt(response.data.notverified.length);
                                $scope.verified = parseInt(response.data.verified.length);
                                $scope.verified = parseInt(response.data.verified.length);
                                $scope.objUserDeposit = response.data.getUserDeposit;
                                $scope.objUserWithdraw = response.data.getUserWithdraw;
                                $scope.objUserWinnings = response.data.getUserWinnings;
                                $scope.objUserFund = response.data.getUserFund;
                                $scope.objUserTotalCashWon = response.data.getUserTotalCashWon;
                                $scope.objUserTotalLeaguesWon = response.data.getUserTotalLeaguesWon;
                                $scope.objUserRecentTransactions = response.data.getUserRecentTransactions;
                                $scope.maximumWithdrawAmount  = response.data.maximumWithdrawAmount;
                                $scope.minimumWithdrawAmount  = response.data.minimumWithdrawAmount;
                                $scope.cashbonous  = response.data.cashbonous;
                                $('.userFund').html('<i class="fa fa-rupee"></i> '+$scope.objUserFund);
                            }else{
                                $('.verifiedDetails').addClass('hide');
                                $('.errorverifydocument').html('Your have not any details yet!');
                                $('.errorverifydocument').removeClass('hide');
                                // $scope.alertMessage();
                            }
                        }, function errorCallback(data) {
                    });
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR EDIT PROFILE
                $scope.userProfile = function(){
                    
                    var id                  = $cookies.get('id');
                    var apikey              = $cookies.get('apikey');
                    $scope.firstname        = $cookies.get('firstname');
                    $scope.middlename       = $cookies.get('middlename');
                    $scope.lastname         = $cookies.get('lastname');
                    $scope.teamname         = $cookies.get('teamname');
                    $scope.phonenumber      = $cookies.get('phonenumber');
                    $scope.dob              = $cookies.get('dob');
                    $scope.gender           = $cookies.get('gender');
                    $scope.address          = $cookies.get('address');
                    $scope.country          = $cookies.get('country');
                    $scope.state            = $cookies.get('state');
                    $scope.city             = $cookies.get('city');
                    $scope.pincode          = $cookies.get('pincode');
                    $scope.apikey           = $cookies.get('apikey');
                    $scope.email            = $cookies.get('email');

                    if ($cookies.get('image') != '') {
                        $scope.image            = $cookies.get('image');
                    }
                    $scope.regEx="/^[0-9]{10,10}$/;";

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-country'
                    }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $scope.objCountry = response.data.getAllCountryObj;
                            }else{
                            }
                        }, function errorCallback(data) {
                    });

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-state/'+$scope.country
                       
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objState = response.data.getAllStateObj;
                        }else{
                        }
                    }, function errorCallback(data) {
                    });

                    window.location = '/app/index.html?#/app/userProfile';  

                }

                if($location.path() == '/app/userProfile' ){  
                    $scope.userProfile();
                }

                
                //FIRE API CALL TO LARAVEL SYSTEM FOR FETCHING THE STATE OF SELECTED COUNTRY
                $scope.getState = function(selectedCountry){

                    $scope.objState = [];
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/fetch-all-state/'+selectedCountry
                       
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objState = response.data.getAllStateObj;
                        }
                    }, function errorCallback(data) {
                    });
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR ABOUT US PAGE
                if($location.path() == '/app/about'){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/page-content/about-us'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.aboutUs = response.data.getPageContentObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }
                    

                //FIRE API CALL TO LARAVEL SYSTEM FOR PRIVACY POLICY
                if($location.path() == '/app/privacyPolicy'){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/page-content/privacy-policy'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.privacyPolicy = response.data.getPageContentObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR JOBS
                if($location.path() == '/app/job'){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/page-content/jobs'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.jobs = response.data.getPageContentObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR FAQ
                if($location.path() == '/app/faq'){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/all-faq-details'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.allFaq       = response.data.listAllFaqCategoryObj;
                            $scope.faqGeneral   = $scope.allFaq[0].content;
                            $scope.scoresPoint  = $scope.allFaq[1].content;
                            $scope.leagues      = $scope.allFaq[2].content;
                            $scope.cashPrizes   = $scope.allFaq[3].content;
                            $scope.verification = $scope.allFaq[4].content;
                        }
                    }, function errorCallback(data) {
                    });
                }
                
                //FIRE API CALL TO LARAVEL SYSTEM FOR TERMS AND CONDITIONS
                if($location.path() == '/app/termsConditions'){ 
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/page-content/terms-conditions'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.terms = response.data.getPageContentObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //FIRE API CALL TO LARAVEL SYSTEM FOR CURRENT REGULATORY FRAMEWORK
                if($location.path() == '/app/legality'){
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/all-legality-details'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.legality = response.data.getLegalityContentDetailsObj;
                        }
                    }, function errorCallback(data) {
                    });
                }
          
                //FIRE API CALL TO LARAVEL SYSTEM FOR POINT SYSTEM
                if($location.path() == '/app/pointSystem'){
                    $http({
                        method: 'Post',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/page-content/point-system'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.pointSystem = response.data.getPageContentObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                //FIRE API CALL TO LATEST NEWS FOR BAT11 GAME
                if($location.path() == '/app/home'){
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        url: $scope.host+'/api/page-content/news-slider'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $window.newsSlider = response.data.getPageContentObj;
                        }
                    }, function errorCallback(data) {
                        
                    });
                }

                $scope.withdraw = {
                    showTooltip: false,
                    tipDirection: 'right'
                };

                $scope.withdraw.delayTooltip = undefined;
                $scope.$watch('withdraw.delayTooltip', function(val) {
                    $scope.withdraw.delayTooltip = parseInt(val, 10) || 0;
                });

		    }
            
            function TabController($scope) {
                $scope.tab = 1;
                $scope.setTab = function(newTab){
                    $scope.tab = newTab;
                    if(newTab == 2){
                        $scope.bankDetails();
                    }else if(newTab == 3){
                        $scope.invitationDetails();
                    }else if(newTab == 5){
                        $scope.withdrawDetails();
                    }
                    console.log(newTab);
                };

                $scope.isSet = function(tabNum){
                    console.log(tabNum);
                  return $scope.tab === tabNum;
                };

            }

            function AccordionDemoCtrl($scope) {
                $scope.oneAtATime = true;
                
                $scope.slider = {
                                min: 0,
                                max: 500,
                                options: {
                                    floor: 0,
                                    ceil: 500
                                }
                            };
                            
                $scope.status = {
                    isCustomHeaderOpen: false,
                    isFirstOpen: true,
                    isFirstDisabled: false
                };
            }

            headerMenuCtrl.$inject = ['$scope', '$http', '$cookies', '$localStorage', '$location', '$rootScope', '$anchorScroll', '$timeout', '$window', '$stateParams'];

            function headerMenuCtrl($scope, $http, $cookies, $localStorage, $location, $rootScope, $anchorScroll, $timeout, $window, $stateParams) {
                //CALL HEADER FUNCTION FOR MENU AND POPUPCALLS
                App.init();

                //TOGGLE MENU NAME IF USER IS LOGGED IN
                if(angular.isUndefined($cookies.get('apikey')) || $cookies.get('apikey') == ''){
                    $('.cd-log_reg ').removeClass('hide');
                    $('.ifUserLoggedIn').addClass('hide');
                    $('.ifUserLoggedIn > .userName').html('');
                    window.location = '/app/index.html?#/app/home';
                    // if($location.path() == '/app/index.html?#/app/dashboard' || $location.path() == '/app/index.html?#/app/userProfile'){
                    // alert('test');
                    // }
                }else{
                    $('.cd-log_reg ').addClass('hide');
                    $('.ifUserLoggedIn').removeClass('hide');
                    $('.ifUserLoggedIn > .ec-navigation-login > ul > li').addClass('');
                    $('.ifUserLoggedIn > .ec-navigation-login > ul > li > .userName').html('<i class="fa fa-chevron-down padding-top5"></i>'+$cookies.get('firstname')+' '+$cookies.get('lastname'));
                    if($location.path() == '/'){
                        window.location = '/app/index.html?#/app/dashboard';
                    }    
                }

                $scope.logoutCall          = function(){
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data:{apikey: $cookies.get('apikey'), id: $cookies.get('id')},
                        url: $scope.host+'/api/logout'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            //REMOVE COOKIES
                            $cookies.remove('id');
                            $cookies.remove('firstname');
                            $cookies.remove('lastname');
                            $cookies.remove('email');
                            $cookies.remove('apikey');

                            $('.cd-log_reg ').removeClass('hide');
                            $('.ifUserLoggedIn').addClass('hide');
                            $('.ifUserLoggedIn > .userName').html('');

                            if($location.path() == '/app/index.html?#/app/dashboard' || $location.path() == '/app/index.html?#/app/userProfile'){      
                                window.location = '/app/index.html?#/app/home';
                            }
                        }else{

                        }
                    }, function errorCallback(response) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
                };
            }

       
            function CarouselSliderCtrl($scope){
              $scope.myInterval = 3000;
              $scope.slides = [
                {
                  image: '../assets/website-content/images/homepageBanner.png',
                  description: 'Welcome to Game of Nerve'
                },
                {
                  image: '../assets/website-content/images/cricket-banner.png',
                  description: 'Welcome to Game of Nerve'
                },
                {
                  image: '../assets/website-content/images/rsz_3banner-3.jpg',
                  description: 'Welcome to Game of Nerve'
                }
              ];
            }

           
            function breakingNewsCtrl($scope){
                $scope.myInterval = 4000;
                $scope.newsSlides = [
                    {
                        news: 'River stumble as Cruzeiro but fly high in table prove'
                    },

                    {
                        news: 'Bresaola alcatra boudin andouille ball tip rump pancetta'
                    },
                    {
                        news: 'Ground round meatball landjaeger tbone chicken'
                    }
                ];

                $scope.item = 6;

            }
           


            function paginationCtrl($scope,$filter){
                
                $scope.currentPage = 0;
                $scope.pageSize = 12;
                $scope.data = [];
                $scope.q = '';
                $scope.arraySize = 0;

                $scope.getData = function () {
                  return $filter('filter')($scope.data, $scope.q)
                }
                
                $scope.numberOfPages=function(){
                    return Math.ceil($scope.getData().length/$scope.pageSize);                
                }
                
                console.log($scope.getData.length);
                for (var i=0; i<65; i++) {
                    $scope.data.push("Item "+i);
                }

            }

            function timerCtrl($scope,$timeout,$interval){
 
                // var future;
            
                // // future = new Date($scope.date);
                // future = new Date();

                // $interval(function() {
                //     var diff;
                //     diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);

                //     $scope.countdown = $scope.dhms(diff);
                //     // return element.text($scope.dhms(diff));
                //   }, 1000);

                // $scope.dhms = function(t){

                //      var hours, minutes, seconds;
                //       hours = Math.floor(t / 3600);
                //       t -= hours * 3600;
                //       minutes = Math.floor(t / 60) % 60;
                //       t -= minutes * 60;
                //       seconds = t % 60;

                //     return [hours + ':', minutes + ':', seconds].join('');

                // }
            }


 
 
})(window.angular);