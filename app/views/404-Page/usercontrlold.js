 //SENDING MOBILE NUMBER FOR OTP FUNCTION STARTS
                $scope.sendOTP  = function(mobile, email){
                    
                    var mobile  = mobile;
                    var email   = email;

                     $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data : {phonenumber : mobile, email : email},
                        url: $scope.host+'/api/send-verified-code'
                       
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){

                            $scope.mobile = response.data.phonenumber;
                            $scope.email = response.data.email;
                            $('.error').removeClass('hide');
                            $('.error').html(response.data.message);
                            $("#ModalOtpSumbit").modal("show");
                            $('#ModalOtpSend').addClass('hide');
                        }else{

                            $('.error').html('Error in sending OTP');
                            $('.error').removeClass('hide');
                        }
                    }, function errorCallback(data) {
                    });

                }//SENDING MOBILE MOBILE NUMBER FOR OTP FUNCTION ENDS 


                //SENDING MOBILE NUMBER FOR OTP FUNCTION STARTS
                $scope.sendOTP  = function(mobile, email){
                    $scope.mobile  = mobile;
                    $scope.email   = email;
                    $('.sendOTPerror').html('');

                     $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data : {phonenumber : mobile, email : email},
                        url: $scope.host+'/api/send-verified-code'
                       
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $cookies.put('mobile',response.data.phonenumber);
                            $cookies.put('email',response.data.email);
                            
                            $('.sendOTPerror').removeClass('hide');
                            $('.sendOTPerror').html(response.data.message);
                            $('input[name=mobile]').val(response.data.phonenumber);
                            $('input[name=email]').val(response.data.email);
                            $("#ModalOtpSumbit").modal("show");
                            $('#ModalOtpSend').addClass('hide');
                        }else{

                            $('.sendOTPerror').html('Error in sending OTP');
                            $('.sendOTPerror').removeClass('hide');
                        }
                    }, function errorCallback(data) {
                    });

                }//SENDING MOBILE MOBILE NUMBER FOR OTP FUNCTION ENDS 