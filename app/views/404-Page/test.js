(function() {
  angular.module('app', [])


  .directive('countdown', [ 'Util', '$interval', function(Util, $interval) {

                        return {
                          restrict: 'A',
                          scope: {
                            date: '@'
                          },
                          link: function(scope, element) {
                            var future;
                            future = new Date(scope.date);
                            $interval(function() {
                              var diff;
                              diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
                              return element.text(Util.dhms(diff));
                            }, 1000);
                          }
                        };
                      }

          ])


$scope.timer          = function($scope,$timeout,$interval){

    var future;

    future = new Date(scope.date);

     $interval(function() {
        var diff;
        diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
        return element.text($scope.dhms(diff));
      }, 1000);

     $scope.dhms = function(t){

         var hours, minutes, seconds;
          hours = Math.floor(t / 3600);
          t -= hours * 3600;
          minutes = Math.floor(t / 60) % 60;
          t -= minutes * 60;
          seconds = t % 60;
          return [hours + ':', minutes + ':', seconds].join('');

     }


}
  
 $scope.stop = function(t){
                   $timeout.cancel(stopped);
                } 




  .factory('Util', [ function() {
      return {
        dhms: function(t) {
          var hours, minutes, seconds;
          hours = Math.floor(t / 3600);
          t -= hours * 3600;
          minutes = Math.floor(t / 60) % 60;
          t -= minutes * 60;
          seconds = t % 60;
          return [hours + ':', minutes + ':', seconds].join('');
        }
      };
    }
  ]);


}).call(this);