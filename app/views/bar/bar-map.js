//COVER FULL PAGE JS
//Google maps api key : AIzaSyCvARVB2eJWR7OM9uCUHzI6Om7jn6du-Jo

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var map;
var infowindow;

function initMap() {
var pyrmont = {lat: 28.5364206, lng: 77.2207574};

map = new google.maps.Map(document.getElementById('map'), {
  center: pyrmont,
  zoom: 15
});

infowindow = new google.maps.InfoWindow();
var service = new google.maps.places.PlacesService(map);
service.nearbySearch({
  location: pyrmont,
  radius: 800,
  type: ['store', 'hotel', 'amusement_park', 'bar', 'cafe', 'night_club', 'casino', 'shopping_mall', 'food']
}, callback);
}

function callback(results, status) {
if (status === google.maps.places.PlacesServiceStatus.OK) {
  for (var i = 0; i < results.length; i++) {
    createMarker(results[i]);
  }
}
}

function createMarker(place) {
var placeLoc = place.geometry.location;
var marker = new google.maps.Marker({
  map: map,
  position: place.geometry.location
});

google.maps.event.addListener(marker, 'click', function() {
  infowindow.setContent(place.name);
  infowindow.open(map, this);
});
}

//DATEPICKER JS
$("#datepicker").on('click', function(){
  alert('clicked');
});
$( "#datepicker" ).datepicker();