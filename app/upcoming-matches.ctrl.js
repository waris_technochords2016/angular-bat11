(function () {
   'use strict';
   angular
      .module('bat11App').controller('MainCtrlUpcomingMatchesSlider', function($scope, $http, $cookies ) {
         var apikey  = $cookies.get('apikey'); 
         var id      = $cookies.get('id'); 
          $http({
                  method: 'POST',
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                  transformRequest: function(obj) {
                     var str = [];
                     for(var p in obj)
                     str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                     return str.join("&");
                  },
                  data:{seasonKey: 'indaus_2017', apikey:apikey, id:id},
                  url: $scope.host+'/api/fetch-season-match'
                  }).then(function successCallback(response) {
                     if(response.data.code == '200'){
                        $scope.items1 = response.data.getSeasonMatchDetailsObj;
                     } 
                  }, function errorCallback(response) {
               });
      // $scope.items2 = [1,2,3,4,5,6,7,8,9,10];
      
      }).directive("owlCarousel", function() {
         return {
            restrict: 'E',
            transclude: false,
            link: function (scope) {
               scope.initCarousel = function(element) {
                  // provide any default options you want
                  var defaultOptions = {
                  };
                  var customOptions = scope.$eval($(element).attr('data-options'));
                     // combine the two options objects
                     for(var key in customOptions) {
                       defaultOptions[key] = customOptions[key];
                     }
                     // init carousel
                     $(element).owlCarousel(defaultOptions);
               };
            }
         };
      })
      
      .directive('owlCarouselItem', [function() {
         return {
            restrict: 'A',
            transclude: false,
            link: function(scope, element) {
               // wait for the last item in the ng-repeat then call init
               if(scope.$last) {
                  scope.initCarousel(element.parent());
               }
            }
         };
      }]);
      
})(window.angular);