(function() {
    'use strict';
    angular
		.module('bat11App')
		.filter('fromNow', fromNow);

		function fromNow() {
		    return function(date) {
		      return moment(date).fromNow();
		    }
		}
})();
