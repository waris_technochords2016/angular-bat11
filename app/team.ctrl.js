/**
 * @ngdoc function
 * @name bat11App.controller:USER AUTHENTICATION CONTROLLER
 * @description
 * # MainCtrl
 * Controller of the bat11App
 */

(function () {
 
	'use strict';
	angular
		.module('bat11App')
			.controller('teamCtrl', teamCtrl)
			.controller('DialogController1', DialogController1)
			// .controller('GreetingController', GreetingController)

			teamCtrl.$inject = ['$scope', '$http', '$cookies', '$localStorage', '$location', '$rootScope', '$anchorScroll', '$timeout', '$window', '$stateParams', '$filter', '$interval', 'uploadFile', '$cookieStore', '$mdDialog'];
			
			function teamCtrl($scope, $http, $cookies, $localStorage, $location, $rootScope, $anchorScroll, $timeout, $window, $stateParams, $filter, $interval, uploadFile, $cookieStore, $mdDialog) {
	
				if($location.host() == 'localhost' || $location.host() == '127.0.0.1' ){
					$scope.host = 'http://localhost:82';
				}else{
					$scope.host = 'http://35.231.238.182:82';
				}

				//FIRE API CALL TO LARAVEL SYSTEM FOR SESSIONS

				if($location.path() == '/app/dashboard' ){  
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						url: $scope.host+'/api/fetch-all-season'
					}).then(function successCallback(response) {
						if(response.data.code == '200'){
							$scope.sessionData = response.data.getSeasonDetailsObj;
							
							if($scope.sessionData.length > 0){
								if(angular.isUndefined($cookies.get('sessionKey')) || $cookies.get('sessionKey') == ''){
									$cookies.put('sessionKey', 'allseason');
									$scope.getMatches('allseason');
								}else{
									$scope.getMatches($cookies.get('sessionKey'));
								}
							}
						}
					}, function errorCallback(data) {
					 
					});
				}

				 
				//FETCH USERS TEAMS
				// $scope.getuserTeamDetails = function(teamId){

				// 	var apikey  = $cookies.get('apikey'); 
				// 	var id      = $cookies.get('id'); 
				// 	$cookies.put('teamId', teamId);

				// 	$http({
				// 		method: 'POST',
				// 		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				// 		transformRequest: function(obj) {
				// 			var str = [];
				// 			for(var p in obj)
				// 			 	str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				// 			return str.join("&");
				// 		},
				// 		data:{apikey: apikey, id:id, teamId:teamId},
				// 		url: $scope.host+'/api/fetch-user-team-details'
				// 	}).then(function successCallback(response) {
				// 		if(response.data.code == '200'){
				// 			$scope.userExistingTeamPlayers = response.data.userExistingTeamDetails[0].players;
				// 			$scope.userTeamName = response.data.userExistingTeamDetails[0].teamName;
				// 			$scope.points = response.data.userExistingTeamDetails[0].points;
				// 		}
				// 	}, function errorCallback(data) {
								
				// 	});

				// }

				//FETCHING THE Teams            
				$scope.getMatches = function(sessionKey){

					$cookies.put('sessionKey', sessionKey);

					$('.series-loader-background').removeClass('hide');
					$('#scroll-match').addClass('hide');

					$scope.countdown 	= [];
					$scope.counter 		= [];
					var apikey  		= $cookies.get('apikey'); 
					var id      		= $cookies.get('id'); 
				 	
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{seasonKey: sessionKey, apikey:apikey, id:id},
						url: $scope.host+'/api/fetch-season-match'
						}).then(function successCallback(response) {
						 	if(response.data.code == '200'){

								$scope.objLeagueMatches = response.data.getSeasonMatchDetailsObj;

								console.log($scope.objLeagueMatches);

								$interval(function() {
									for (var i = 0; i < $scope.objLeagueMatches.length; i++) {
										$scope.countdown [i] = $scope.getTimeFormat($scope.objLeagueMatches[i].startDate);
									}      
								}, 1000);    

								$interval(function() {
									for (var i = 0; i < $scope.objLeagueMatches.length; i++) {
										$scope.counter [i] = $scope.getCounter($scope.objLeagueMatches[i].startDate);
									}      
								}, 1000);

								$cookies.put('checkCookies',true);

								for (var i = 0; i < $scope.objLeagueMatches.length; i++) {
									var future = 0,  t = 0;
									future = new Date($scope.objLeagueMatches[i].startDate);
									t = Math.floor((future.getTime() - new Date().getTime()) / 1000);

									if(t > 0)  {
										if(angular.isUndefined($cookies.get('matchId')) || $cookies.get('matchId') == ''){
											$scope.getBetOptions($scope.objLeagueMatches[i].id, $scope.objLeagueMatches[i].matchKey);
											break;
										}else{
											$scope.getBetOptions($cookies.get('matchId'),$cookies.get('matchKey'));
											break;
										}
									}
								}

								$('.series-loader-background').addClass('hide');
								$('#scroll-match').removeClass('hide');

						 	} 

						}, function errorCallback(response) {
					});

				}
			
				
				if($location.path() == '/app/test' ){  
					window.location = '/app/index.html?#/app/dashboard'; 
				} 

				$scope.getTimeFormat = function(time){
					var hours = 0, minutes = 0, seconds = 0, t = 0;
					var future;    
					future 		= new Date(time);
					t 			= Math.floor((future.getTime() - new Date().getTime()) / 1000);
					hours 		= Math.floor(t / 3600);
					t 			-= hours * 3600;
					minutes 	= Math.floor(t / 60) % 60;
					t 			-= minutes * 60;
					seconds 	= t % 60;
					return [hours + ':', minutes + ':', seconds].join('');
				}

				$scope.getContestTimeFormat = function(time){
					var hours = 0, minutes = 0, seconds = 0, t = 0;
					var future;    
					future 		= new Date(time);
					t 			= Math.floor((future.getTime() - new Date().getTime()) / 1000);
					hours 		= Math.floor(t / 3600);
					t 			-= hours * 3600;
					minutes 	= Math.floor(t / 60) % 60;
					t 			-= minutes * 60;
					seconds 	= t % 60;
					return [hours + 'h', minutes + 'm', seconds+'s'].join(' ');
				}

				$scope.getCounter = function(time){
					var future = 0,  t = 0;
					future = new Date(time);
					t = Math.floor((future.getTime() - new Date().getTime()) / 1000);
					return [t];
				}

				//FETCHING THE Bet for the selected match           
				$scope.getBetOptions = function(matchId, matchKey){

					var panel = '#match-panel'+matchId;
					$(panel).removeClass('panel-primary-match');
					$(panel).addClass('match-active');

					$('.bet-loader-background').removeClass('hide');
					$('.teamLoader').removeClass('hide');

					$('.match-loader-background').removeClass('hide');
					$('#current-match-section').addClass('hide');


					$('#bet-section').addClass('hide');
					$('.create-team-section').addClass('hide');

					$cookies.put('matchId',matchId);
					$cookies.put('matchKey',matchKey);
						 
					var apikey            = $cookies.get('apikey'); 
					var id                = $cookies.get('id'); 
					// var sessionKey     = $cookies.get('sessionKey'); 
 
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id: id, apikey: apikey, matchId: matchId, matchKey: matchKey},
						url: $scope.host+'/api/fetch-match-bet'
					}).then(function successCallback(response) {
					
						if(response.data.code == '200'){
							$('.bet-loader-background').addClass('hide');
							$('#bet-section').removeClass('hide');
							$('.create-team-section').removeClass('hide');
							
							$(panel).removeClass('panel-primary-match');
							$(panel).addClass('match-active');

							$scope.objBetDetails 	= response.data.leagues_array_bet;
							$scope.objMatchDetails 	= response.data.leagues_array_match;

							$interval(function() {
								$scope.matchCountdown  = $scope.getTimeFormat($scope.objMatchDetails[0]['startDate']);
							}, 1000);    

							$interval(function() {
								$scope.matchCounter = $scope.getCounter($scope.objMatchDetails[0]['startDate']);
							}, 1000);

							var list = []; 
							$cookies.put('teamaId', $scope.objMatchDetails[0]['teamaId']);
							$cookies.put('teambId', $scope.objMatchDetails[0]['teambId']);
							$cookieStore.put('listPlayer', list);

							$('.teamLoader').addClass('hide');
							$('.loader-background').addClass('hide');
							$('.loader-body').removeClass('hide');

							$('#current-match-section').removeClass('hide');
							$('.match-loader-background').addClass('hide');
							
							$('.vertical-table').removeClass('hide');
						}
					}, function errorCallback(response) {
					});

				}

				//FETCHING the user conte           
				$scope.getmatchPlayer = function(matchId, matchKey){

					var panel = '#match-panel'+matchId;
					$(panel).removeClass('panel-primary-match');
					$(panel).addClass('match-active');

					$('.bet-loader-background').removeClass('hide');
					$('.teamLoader').removeClass('hide');

					$('.match-loader-background').removeClass('hide');
					$('#current-match-section').addClass('hide');


					$('#bet-section').addClass('hide');
					$('.create-team-section').addClass('hide');

					$cookies.put('matchId',matchId);
					$cookies.put('matchKey',matchKey);

					$cookies.remove('teamaPlayer');
					$cookies.remove('teambPlayer');
					$cookies.remove('wicketKeeper');
					$cookies.remove('batsman');
					$cookies.remove('bowler');
					$cookies.remove('allrounder');
					$cookies.remove('totalPoints');
					$cookies.remove('spentPoints');
					$cookies.remove('remainingPoints');
					$cookies.remove('playerCount');

					$cookies.put('teamaPlayer', 0);
					$cookies.put('teambPlayer', 0);
					$cookies.put('wicketKeeper', 0);
					$cookies.put('batsman', 0);
					$cookies.put('bowler', 0);
					$cookies.put('allrounder', 0);
					$cookies.put('totalPoints', 100);
					$cookies.put('spentPoints', 0);
					$cookies.put('remainingPoints', 100);
					$cookies.put('playerCount', 0);
						 
					var apikey            = $cookies.get('apikey'); 
					var id                = $cookies.get('id'); 

					$scope.objAllPlayer   = [];
					$scope.objBatsman     = [];
					$scope.objBowler      = [];
					$scope.objWicketKeeper= [];
					$scope.objAllRounder  = [];

					$scope.player1 = '';
					$scope.player2 = '';
					$scope.player3 = '';
					$scope.player4 = '';
					$scope.player5 = '';
					$scope.player6 = '';
					$scope.player7 = '';
					$scope.player8 = '';
					$scope.player9 = '';
					$scope.player10 = '';
					$scope.player11 = '';

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id: id, apikey: apikey, matchId: matchId, matchKey: matchKey},
						url: $scope.host+'/api/fetch-match-team-players'
					}).then(function successCallback(response) {
					
						if(response.data.code == '200'){
							$('.bet-loader-background').addClass('hide');
							$('#bet-section').removeClass('hide');
							$('.create-team-section').removeClass('hide');
							
							
							$(panel).removeClass('panel-primary-match');
							$(panel).addClass('match-active');

							// $scope.objBetDetails =  response.data.leagues_array_bet;
							$scope.objMatchDetails =  response.data.leagues_array_match;

							for (var i = 0; i < $scope.objMatchDetails[0]['player'].length; i++) {
								$scope.objAllPlayer.push($scope.objMatchDetails[0]['player'][i]); 
							}

							for (var i = 0; i < $scope.objMatchDetails[0]['player'].length; i++) {
							 	if($scope.objMatchDetails[0]['player'][i].playingRole == 'all-rounder'){
									$scope.objAllRounder.push($scope.objMatchDetails[0]['player'][i]);
								}
							}

							for (var i = 0; i < $scope.objMatchDetails[0]['player'].length; i++) {
								if($scope.objMatchDetails[0]['player'][i].playingRole == 'Batsman'){
									$scope.objBatsman.push($scope.objMatchDetails[0]['player'][i]);
								}
							}

							for (var i = 0; i < $scope.objMatchDetails[0]['player'].length; i++) {
								if($scope.objMatchDetails[0]['player'][i].playingRole == 'Bowler'){
									$scope.objBowler.push($scope.objMatchDetails[0]['player'][i]);
								}
							}

							for (var i = 0; i < $scope.objMatchDetails[0]['player'].length; i++) {
								if($scope.objMatchDetails[0]['player'][i].playingRole == 'wicket-keeper'){
									$scope.objWicketKeeper.push($scope.objMatchDetails[0]['player'][i]);
								}
							}

							$interval(function() {
								$scope.matchCountdown  = $scope.getTimeFormat($scope.objMatchDetails[0]['startDate']);
							}, 1000);    

							$interval(function() {
								$scope.matchCounter = $scope.getCounter($scope.objMatchDetails[0]['startDate']);
							}, 1000);

							var list = []; 
							$cookies.put('teamaId', $scope.objMatchDetails[0]['teamaId']);
							$cookies.put('teambId', $scope.objMatchDetails[0]['teambId']);
							$cookieStore.put('listPlayer', list);

							$('.teamLoader').addClass('hide');
							$('.loader-background').addClass('hide');
							$('.loader-body').removeClass('hide');

							$('#current-match-section').removeClass('hide');
							$('.match-loader-background').addClass('hide');
							
							$('.vertical-table').removeClass('hide');
						}
					}, function errorCallback(response) {

					});

				}

				if($location.path() == '/app/createTeam' ){  
					var matchId  = $cookies.get('matchId');
					var matchKey = $cookies.get('matchKey');
					$scope.getmatchPlayer(matchId,matchKey);
				} 

				if($location.path() == '/app/mycontest'){
				 
					var apikey            			= $cookies.get('apikey'); 
					var id                			= $cookies.get('id'); 
					$scope.betJoinedUpcomingMatches = [];
					$scope.betJoinedLiveMatches 	= [];
					$scope.betJoinedArchiveMatches 	= [];
					$scope.countdownUpcoming 		= [];
					 
					
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id: id, apikey: apikey},
						url: $scope.host+'/api/fetch-user-contest-matches'
					}).then(function successCallback(response) {
						if(response.data.code == '200'){
							$('.bet-loader-background').addClass('hide');
							$('.user-contest-section').removeClass('hide');

							var betJoinedMatches = response.data.betJoinedMatches;
							var date = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss ');
 
							for (var i = 0;  i < betJoinedMatches.length ;  i++) {

								if(betJoinedMatches[i]['startDate'] >  date){
									$scope.betJoinedUpcomingMatches.push(betJoinedMatches[i]); 
								}else if(betJoinedMatches[i]['startDate'] <  date && betJoinedMatches[i]['matchStatus'] == 'completed'){
									$scope.betJoinedArchiveMatches.push(betJoinedMatches[i]); 
								}else{
									$scope.betJoinedLiveMatches.push(betJoinedMatches[i]); 
								}
 
								$('#upcoming-matches').html('Upcoming ('+$scope.betJoinedUpcomingMatches.length+')' );
								$('#live-matches').html('Live ('+$scope.betJoinedLiveMatches.length+')');
								$('#archive-matches').html('Archive ('+$scope.betJoinedArchiveMatches.length+')');
							}

							$interval(function() {
								for (var i = 0; i < $scope.betJoinedUpcomingMatches.length; i++) {
									$scope.countdownUpcoming [i] = $scope.getContestTimeFormat($scope.betJoinedUpcomingMatches[i].startDate);
								}      
							}, 1000);   

							  
							 

						}else{

						}
					}, function errorCallback(response) {

					});
				 
				}

				$scope.changeMatch = function(matchId, matchKey){
					$('.panel-primary').addClass('panel-primary-match');
					$('.panel-primary').removeClass('match-active');
					$scope.getBetOptions(matchId, matchKey);
				}

				$scope.viewUserContest = function(matchId){
					var apikey    = $cookies.get('apikey'); 
					var id        = $cookies.get('id'); 

					$cookies.put('contestMatchId', matchId);

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id: id, apikey: apikey, matchId: matchId},
						url: $scope.host+'/api/fetch-user-contest'
					}).then(function successCallback(response) {
						if(response.data.code == '200'){

							$scope.userContest	= response.data.betJoinedContest;
							$scope.contestMatch 	= response.data.betContestMatch;

							console.log($scope.contestMatch[0]['startDate']);
							$interval(function() {
								$scope.contestMatchCountdown  = $scope.getTimeFormat($scope.contestMatch[0]['startDate']);
							}, 1000);    

							$interval(function() {
								$scope.contestMatchCounter = $scope.getCounter($scope.contestMatch[0]['startDate']);
							}, 1000);

							window.location = '/app/index.html?#/app/matchcontestbets';

						}else{

						}

					}, function errorCallback(response) {

					});

				}
 				

 				$scope.viewUserContestTeam = function(matchId, betId, slotIndex){
					
					var apikey    				= $cookies.get('apikey'); 
					var id        				= $cookies.get('id'); 
					$scope.getUserContestTeams 	= [];
					$scope.contestDetails 		= [];

					$cookies.put('contestMatchId', matchId);
					$cookies.put('contestId', betId);
					$cookies.put('slotIndex', slotIndex);

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id: id, apikey: apikey, matchId: matchId, betId: betId, slotIndex: slotIndex},
						url: $scope.host+'/api/fetch-user-contest-team'
					}).then(function successCallback(response) {
						if(response.data.code == '200'){

							$scope.getUserContestTeams	= response.data.betJoinedContestTeam;
							$scope.contestDetails		= response.data.contestDetails;
							$scope.contestMatch 		= response.data.betContestMatch;

							$interval(function() {
								$scope.contestMatchCountdown  = $scope.getTimeFormat($scope.contestMatch[0]['startDate']);
							}, 1000);    

							$interval(function() {
								$scope.contestMatchCounter = $scope.getCounter($scope.contestMatch[0]['startDate']);
							}, 1000);

							window.location = '/app/index.html?#/app/mycontestteams';

						}

					}, function errorCallback(response) {

					});

				}

				$scope.viewUserContestTeamPlayer = function(matchId, betId, slotIndex, teamId, userId){
					var apikey    = $cookies.get('apikey'); 
					var id        = $cookies.get('id'); 

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id: id, apikey: apikey, matchId: matchId, betId: betId, slotIndex: slotIndex, teamId: teamId, userId: userId},
						url: $scope.host+'/api/fetch-user-contest-team-player'
					}).then(function successCallback(response) {
						if(response.data.code == '200'){

							$scope.userContestTeamPlayer	= response.data.userTeamPlayer;

							$('.bet-player').removeClass('hide');

							for (var i = 0;  i < $scope.userContestTeamPlayer.length ;  i++) {
								if(i == 0){
									$scope.player0 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player0_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 1){
									$scope.player1 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player1_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 2){
									$scope.player2 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player2_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 3){
									$scope.player3 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player3_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 4){
									$scope.player4 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player4_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 5){
									$scope.player5 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player5_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 6){
									$scope.player6 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player6_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 7){
									$scope.player7 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player7_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 8){
									$scope.player8 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player8_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 9){
									$scope.player9 			= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player9_point 	= $scope.userContestTeamPlayer[i]['points'];
								}else if(i == 10){
									$scope.player10 		= $scope.userContestTeamPlayer[i]['playerImage'];
									$scope.player10_point 	= $scope.userContestTeamPlayer[i]['points'];
								}	
							}

						}

					}, function errorCallback(response) {

					});

				}

				if($location.path() == '/app/matchcontestbets'){
					var contestMatchId = $cookies.get('contestMatchId');
					$scope.viewUserContest(contestMatchId);
				}

				if($location.path() == '/app/mycontestteams'){
					var contestMatchId 	= $cookies.get('contestMatchId');
					var contestId 		= $cookies.get('contestId');
					var slotIndex 		= $cookies.get('slotIndex');
					
					$scope.viewUserContestTeam(contestMatchId, contestId, slotIndex);
				}

				//SEND BET JOINING REQUEST            
				$scope.joinBet = function(ev, betId, playamount, availableSlotIndex){

					var id      		= $cookies.get('id'); 
					var apikey  		= $cookies.get('apikey'); 
					var sessionKey 		= $cookies.get('sessionKey')
					var matchId 		= $cookies.get('matchId');  

					$scope.user_fund 	= 0;
					$scope.user_teams 	= [];

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							 var str = [];
							 for(var p in obj)
							 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							 return str.join("&");
						},
						data:{id: id, apikey: apikey,matchId: matchId},
						url: $scope.host+'/api/fetch-user-teams'
						}).then(function successCallback(response) {
							if(response.data.code == '200'){
								
								$scope.user_fund 	= response.data.user_fund.fund;
								$scope.user_teams 	= response.data.userExistingTeam;

								if($scope.user_fund < playamount ){
									// Add fund because user have insufficient balance in their wallet
									$mdDialog.show({
										controller: DialogController1,
										templateUrl: 'addFundToJoin.tmpl',
										parent: angular.element(document.body),
										targetEvent: ev,
										clickOutsideToClose:true,
										locals: { error : $scope.user_teams}
									})
									.then(function(answer) {
										$scope.status = '';
									}, function() {
										$scope.status = 'You cancelled the dialog.';
									});
									
								}else{

									if($scope.user_teams.length > 1){

										/***************************************************************************
										// Select team as user have created more than 1 team for this match
										***************************************************************************/
										$mdDialog.show({
											controller: DialogController1,
											templateUrl: 'selectTeamToJoin.tmpl',
											parent: angular.element(document.body),
											targetEvent: ev,
											clickOutsideToClose:true,
											locals: { error : $scope.user_teams, betId : betId }
										})
										.then(function(answer) {
											$scope.status = '';
										}, function() {
											$scope.status = 'You cancelled the dialog.';
										});

									}else{

										/******************************************************************
										// Save User bet for the selected match & with the selected team
										******************************************************************/
										var teamId = response.data.userExistingTeam[0]['id'];
										$scope.saveLeagueJoining(1, betId, availableSlotIndex, teamId);
									}
								}

							}else{

								/**********************************************************************************************
									Show notification to creating team as user doesn't have created any team for that match 
								 	and have a button to redirected user to the team creation page
								***********************************************************************************************/
								
								$mdDialog.show({
									controller: DialogController1,
									templateUrl: 'createTeamToJoin.tmpl',
									parent: angular.element(document.body),
									targetEvent: ev,
									clickOutsideToClose:true,
									locals: { error : response.data.code}
								})
								.then(function(answer) {
									$scope.status = '';
								}, function() {
									$scope.status = 'You cancelled the dialog.';
								});
							} 

						}, function errorCallback(response) {
					});

				}

				$scope.saveLeagueJoining = function(ev, betId, availableSlotIndex, teamId){

					var id      		= $cookies.get('id'); 
					var apikey  		= $cookies.get('apikey'); 
					var sessionKey 		= $cookies.get('sessionKey')
					var matchId 		= $cookies.get('matchId');  

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							var str = [];
							for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							return str.join("&");
						},
						data:{id : id, apikey : apikey, sessionKey : sessionKey, matchId : matchId, betId : betId, availableSlotIndex: availableSlotIndex, teamId : teamId},
						url: $scope.host+'/api/join-user-bet'
						}).then(function successCallback(response) {
						 	if(response.data.code == '200'){

						 		// Show successfull message for successfully bet joining
								$mdDialog.show({
									controller: DialogController1,
									templateUrl: 'successJoinBet.tmpl',
									parent: angular.element(document.body),
									targetEvent: ev,
									clickOutsideToClose:true,
									locals: { error : response.data.message}
								})
								.then(function(answer) {
									$scope.status = '';
								}, function() {
									$scope.status = 'You cancelled the dialog.';
								});

								var matchId  = $cookies.get('matchId');
								var matchKey = $cookies.get('matchKey');
								$scope.changeMatch(matchId, matchKey);

						 	}else{

						 		// Show Error message for unsuccessfully bet joining
								$mdDialog.show({
									controller: DialogController1,
									templateUrl: 'successJoinBet.tmpl',
									parent: angular.element(document.body),
									targetEvent: ev,
									clickOutsideToClose:true,
									locals: { error : response.data.message}
								})
								.then(function(answer) {
									$scope.status = '';
								}, function() {
									$scope.status = 'You cancelled the dialog.';
								});
						 	} 

						}, function errorCallback(response) {
					});
				}

				//FETCHING VIEW POINTS            
				$scope.viewPoints = function(matchId, matchKey){
					var apikey  = $cookies.get('apikey'); 
					var id      = $cookies.get('id'); 
					window.location = '/app/index.html?#/app/mycontest';  
				}

				//FETCH USERS TEAMS
				// $scope.getUserExistingTeams = function(){

				// 	var apikey  	= $cookies.get('apikey'); 
				// 	var id      	= $cookies.get('id'); 
				// 	var matchId 	= $cookies.get('matchId',matchId);
				// 	var matchKey 	= $cookies.get('matchKey',matchKey);

				// 	$http({
				// 			method: 'POST',
				// 			headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				// 			transformRequest: function(obj) {
				// 				 var str = [];
				// 				 for(var p in obj)
				// 				 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				// 				 return str.join("&");
				// 			},
				// 			data:{apikey: apikey, id:id, matchId: matchId, matchKey: matchKey},
				// 			url: $scope.host+'/api/fetch-user-teams'
				// 	 }).then(function successCallback(response) {
				// 			if(response.data.code == '200'){
				// 				$scope.userExistingTeam 	= response.data.userExistingTeam;
				// 				$scope.userTeamName 		= $scope.userExistingTeam[0].teamName;
				// 				$scope.userTeamid 			= $scope.userExistingTeam[0].id;
				// 				$scope.points 				= $scope.userExistingTeam[0].points;
				// 				$scope.getuserTeamDetails($scope.userTeamid);
				// 			}else{
				// 			}
				// 	 }, function errorCallback(data) {
								
				// 	 });
				// }

				//ADDING PLAYERS SECTION IN CREATING TEAM 
				$scope.addPlayer          = function(playerId, teamId, playingRole, playerCredits, playerName, image){

					 var error               = false;
					 var parentClass         = '.'+playingRole+''+playerId+' > .circle-add';
					 var childClass          = '.'+playingRole+''+playerId+' > .circle-minus';
					 var wicketKeeper        = 0;
					 var batsman             = 0;
					 var allrounder          = 0;
					 var bowler              = 0;
					 var teamaPlayer         = 0;
					 var teambPlayer         = 0;
					 var spentPoints         = 0;
					 var remainingPoints     = 0;
					 var playerCount         = 0;
					 var addListPlayer       = [];
				

					 if(parseInt($cookies.get('remainingPoints')) <  parseInt(playerCredits) ){
							$('.creditShow').addClass('hide');
							$('.creditError').removeClass('hide');
							$('.creditError > span').html('No Credits Left');
							error = true;
							$scope.showTeamCreationError('event','No Credit Left');
					 }else if((parseInt($cookies.get('playerCount'))) >= 11) {
							$('.playerShow').addClass('hide');
							$('.playerError').removeClass('hide');
							$('.playerError > span').html('Maximum 11 players can be added');
							error = true;
							$scope.showTeamCreationError('event','Maximum 11 players can be added');
					 }else if((teamId == $cookies.get('teamaId')) && (parseInt($cookies.get('teamaPlayer')) >= 7)){
							error = true;
							$scope.showTeamCreationError('event','you can choose maximum 7 players from one team');
					 }else if((teamId == $cookies.get('teambId')) && (parseInt($cookies.get('teambPlayer')) >= 7)){
							error = true;
							$scope.showTeamCreationError('event','you can choose maximum 7 players from one team');
					 }else if((playingRole == 'wicket-keeper') && (parseInt($cookies.get('wicketKeeper')) >= 1)) {
							$scope.showTeamCreationError('event','You can choose only one Wicket Keeper');  
							error = true;
					 }else if((playingRole == 'Batsman') && (parseInt($cookies.get('batsman')) >= 5) )   {
							error = true; 
							$scope.showTeamCreationError('event','Not More than 5 Batsman can be choose');  
					 }else if((playingRole == 'all-rounder') && (parseInt($cookies.get('allrounder')) >= 3)) {
							error = true;
							$scope.showTeamCreationError('event','Not more than 3 All Rounder can be choose');  
					 }else if((playingRole == 'Bowler') && (parseInt($cookies.get('bowler')) >= 5)){
							error = true;
							$scope.showTeamCreationError('event','Not more than 5 Bowler can be choose');  
					 }else if(parseInt($cookies.get('playerCount')) == 10 && (playingRole != 'wicket-keeper') && (parseInt($cookies.get('wicketKeeper')) < 1)){
								$scope.showTeamCreationError('event','Atleast 1 Wicket Keeper need to be choosed');  
							error = true;
					 }else if(parseInt($cookies.get('playerCount')) == 10 && (playingRole != 'Batsman') && (parseInt($cookies.get('batsman')) < 3)){
							$scope.showTeamCreationError('event','Minimum 3 Batsman need to be choosed');  
							error = true;
					 }else if(parseInt($cookies.get('playerCount')) == 10 && (playingRole == 'Batsman') && ((parseInt($cookies.get('batsman')) + 1 ) < 3)){
							$scope.showTeamCreationError('event','Minimum 3 Batsman need to be choosed');    
							error = true;
					 }else if(parseInt($cookies.get('playerCount')) == 10 && (playingRole != 'all-rounder') && (parseInt($cookies.get('allrounder')) < 1)){
							$scope.showTeamCreationError('event','Minimum 1 All Rounder need to be choosed');    
							error = true;
					 }else if(parseInt($cookies.get('playerCount')) == 10 && (playingRole == 'Bowler') && ((parseInt($cookies.get('bowler')) + 1) < 3)){
							$scope.showTeamCreationError('event','Minimum 3 Bowlers need to be choosed');    
							error = true;
					 }else if(parseInt($cookies.get('playerCount')) == 10 && (playingRole != 'Bowler') && (parseInt($cookies.get('bowler')) < 3)){
							$scope.showTeamCreationError('event','Minimum 3 Bowlers need to be choosed');    
							error = true;
					 }

					 if(error == false){

							if(playingRole == 'wicket-keeper'){
								 wicketKeeper     = parseInt($cookies.get('wicketKeeper')) + 1;
								 $cookies.put('wicketKeeper', wicketKeeper);
								 $('#numWK').html('WK ('+wicketKeeper+')');
							}else if(playingRole == 'Batsman'){
								 batsman          = parseInt($cookies.get('batsman')) + 1;
								 $cookies.put('batsman', batsman);
								 $('#numBatsman').html('Batsman ('+batsman+')');
							}else if(playingRole == 'all-rounder'){
								 allrounder       = parseInt($cookies.get('allrounder')) + 1;
								 $cookies.put('allrounder', allrounder);
								 $('#numAllRounder').html('All-Rounder ('+allrounder+')');
							}else if(playingRole == 'Bowler'){
								 bowler           = parseInt($cookies.get('bowler')) + 1;
								 $cookies.put('bowler', bowler);
								 $('#numBowlers').html('Bowlers ('+bowler+')');
							} 
							
							if(teamId == $cookies.get('teamaId')){
								 teamaPlayer      = parseInt($cookies.get('teamaPlayer')) + 1;
								 $cookies.put('teamaPlayer', teamaPlayer);
							}else if(teamId == $cookies.get('teambId')){
								 teambPlayer      = parseInt($cookies.get('teambPlayer')) + 1;
								 $cookies.put('teambPlayer', teambPlayer);
							}

							spentPoints = parseInt($cookies.get('spentPoints')) + parseInt( playerCredits);
							$cookies.put('spentPoints', spentPoints);

							remainingPoints = parseInt($cookies.get('totalPoints')) - parseInt(spentPoints);
							$cookies.put('remainingPoints', remainingPoints);

							playerCount = parseInt($cookies.get('playerCount')) + 1;
							$cookies.put('playerCount', playerCount); 

							addListPlayer   = $cookieStore.get('listPlayer');

							addListPlayer.push({id:playerId, name:playerName});
							$cookieStore.put('listPlayer', addListPlayer);

							$('.playerShow').removeClass('hide');
							$('.playerError').addClass('hide');
							$('.playerShow > span').html(playerCount+'/11');
							$('.creditShow').removeClass('hide');
							$('.creditError').addClass('hide');
							$('.totalPoints').html('Total Credit (100) -');
							$('.creditSpent').html('Credit Spent ('+spentPoints+') = ');
							$('.creditLeft').html('Credits Remaining ('+ remainingPoints+')');
							$(parentClass).addClass('hide');
							$(childClass).removeClass('hide');
							$('#numALL').html('All ('+playerCount+')');

							if(parseInt($cookies.get('playerCount')) == 1){
								$('.player1').removeClass('hide');
								$scope.player1 = image;
							}else if(parseInt($cookies.get('playerCount')) == 2){
								$('.player2').removeClass('hide');
								$scope.player2 = image;
							}else if(parseInt($cookies.get('playerCount')) == 3){
								$('.player3').removeClass('hide');
								$scope.player3 = image;
							} else if(parseInt($cookies.get('playerCount')) == 4){
								$('.player4').removeClass('hide');
								$scope.player4 = image;
							}else if(parseInt($cookies.get('playerCount')) == 5){
								$('.player5').removeClass('hide');
								$scope.player5 = image;
							} else if(parseInt($cookies.get('playerCount')) == 6){
								$('.player6').removeClass('hide');
								$scope.player6 = image;
							}else if(parseInt($cookies.get('playerCount')) == 7){
								$('.player7').removeClass('hide');
								$scope.player7 = image;
							}else if(parseInt($cookies.get('playerCount')) == 8){
								$('.player8').removeClass('hide');
								$scope.player8 = image;
							}else if(parseInt($cookies.get('playerCount')) == 9){
								$('.player9').removeClass('hide');
								$scope.player9 = image;
							}else if(parseInt($cookies.get('playerCount')) == 10){
								$('.player10').removeClass('hide');
								$scope.player10 = image;
							}else if(parseInt($cookies.get('playerCount')) == 11){
								$('.player11').removeClass('hide');
								$scope.player11 = image;
							}

							if(parseInt($cookies.get('playerCount')) == 11){
								 $scope.chooseCaptainViceCaptain();
							}

					 }    

				}

				//REMOVING PLAYERS SECTION IN CREATING TEAM 
				$scope.removePlayer          = function(playerId, teamId, playingRole, playerCredits, playerName, image){

					 var error               = false;
					 var parentClass         = '.'+playingRole+''+playerId+' > .circle-add';
					 var childClass          = '.'+playingRole+''+playerId+' > .circle-minus';
					 var wicketKeeper        = 0;
					 var batsman             = 0;
					 var allrounder          = 0;
					 var bowler              = 0;
					 var teamaPlayer         = 0;
					 var teambPlayer         = 0;
					 var spentPoints         = 0;
					 var remainingPoints     = 0;
					 var playerCount         = 0;
					 var removeListPlayer    = [];

					 removeListPlayer              = $cookieStore.get('listPlayer');

					 for (var i = 0; i < removeListPlayer.length; i++) {
							if(removeListPlayer[i].id == playerId){
								 removeListPlayer.splice(i, 1);
							}   
					 }

					 $cookieStore.put('listPlayer', removeListPlayer);

					 if(playingRole == 'wicket-keeper'){
								 wicketKeeper     = parseInt($cookies.get('wicketKeeper')) - 1;
								 $cookies.put('wicketKeeper', wicketKeeper);
								 $('#numWK').html('WK ('+wicketKeeper+')');
							}else if(playingRole == 'Batsman'){
								 batsman          = parseInt($cookies.get('batsman')) - 1;
								 $cookies.put('batsman', batsman);
								 $('#numBatsman').html('Batsman ('+batsman+')');
							}else if(playingRole == 'all-rounder'){
								 allrounder       = parseInt($cookies.get('allrounder')) - 1;
								 $cookies.put('allrounder', allrounder);
								 $('#numAllRounder').html('All-Rounder ('+allrounder+')');
							}else if(playingRole == 'Bowler'){
								 bowler           = parseInt($cookies.get('bowler')) - 1;
								 $cookies.put('bowler', bowler);
								 $('#numBowlers').html('Bowlers ('+bowler+')');
							} 

					 if(teamId == $cookies.get('teamaId')){
								 $scope.teamaPlayer      = parseInt($cookies.get('teamaPlayer')) - 1;
								 $cookies.put('teamaPlayer', $scope.teamaPlayer);
					 }else if(teamId == $cookies.get('teambId')){
								 teambPlayer      = parseInt($cookies.get('teambPlayer')) - 1;
								 $cookies.put('teambPlayer', teambPlayer);
					 }

					 spentPoints = parseInt($cookies.get('spentPoints')) - parseInt(playerCredits);
					 $cookies.put('spentPoints', spentPoints);

					 remainingPoints = parseInt($cookies.get('remainingPoints')) + parseInt(playerCredits);
					 $cookies.put('remainingPoints', remainingPoints);

					 playerCount = parseInt($cookies.get('playerCount')) - 1;
					 $cookies.put('playerCount', playerCount); 

					 $('.playerShow').removeClass('hide');
					 $('.playerError').addClass('hide');
					 $('.playerShow > span').html(playerCount+'/11');
					 $('.creditShow').removeClass('hide');
					 $('.creditError').addClass('hide');
					 $('.totalPoints').html('Total Credit (100) -');
					 $('.creditSpent').html('Credit Spent ('+$cookies.get('spentPoints')+') = ');
					 $('.creditLeft').html('Credits Remaining ('+$cookies.get('remainingPoints')+')');

					 $('#numALL').html('All ('+playerCount+')');

					 $(parentClass).removeClass('hide');
					 $(childClass).addClass('hide');
						 
				}

				// BETS FILTERING SECTION
				$scope.reset          = function(){
					 window.location = '/app/index.html?#/app/dashboard';  
				}

				$scope.filterByTeamSize          = function(){
					 window.location = '/app/index.html?#/app/dashboard';  
				}

				$scope.filterByPay          = function(){
					 window.location = '/app/index.html?#/app/dashboard';  
				}

				$scope.filterByWin          = function(){
					 window.location = '/app/index.html?#/app/dashboard';  
				}

				$scope.showTeamCreationError = function(event, teamError) {
						$mdDialog.show({
							targetEvent: event,
							templateUrl: 'error.tmpl',
							controller: 'DialogController1',
							clickOutsideToClose:true,
							onComplete: $scope.afterShowAnimation,
							locals: { error : teamError }
					 });

					 // When the 'enter' animation finishes...
					 $scope.afterShowAnimation = function(scope, element, options) {
							// post-show code here: DOM element focus, etc.
					 }

				} // END showCustomGreeting FUNCTION

				$scope.chooseCaptainViceCaptain = function(ev, teamError) {
					var objPlayerList   = [];
					objPlayerList 		= $cookieStore.get('listPlayer');

					$mdDialog.show({
						controller: DialogController1,
						templateUrl: 'testCaptain.tmpl',
						parent: angular.element(document.body),
						targetEvent: ev,
						clickOutsideToClose:true,
						locals: { error : objPlayerList}
					})
					.then(function(answer) {
						$scope.status = '';
					}, function() {
						$scope.status = 'You cancelled the dialog.';
					});

				}; //END OF chooseCaptainViceCaptain function


				$scope.joinMatch = function(matchId){

					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
						transformRequest: function(obj) {
							 var str = [];
							 for(var p in obj)
							 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
							 return str.join("&");
						},
						data:{apikey: apikey, id:id, matchId: matchId, matchKey: matchKey},
						url: $scope.host+'/api/fetch-user-teams'
					 }).then(function successCallback(response) {
						if(response.data.code == '200'){
							$scope.userExistingTeam 	= response.data.userExistingTeam;
							$scope.userTeamName 		= $scope.userExistingTeam[0].teamName;
							$scope.userTeamid 			= $scope.userExistingTeam[0].id;
							$scope.points 				= $scope.userExistingTeam[0].points;
							$scope.getuserTeamDetails($scope.userTeamid);
						}
					 }, function errorCallback(data) {
								
					 });

				}

			}// END OF teamCtrl 


			DialogController1.$inject = ['$scope', '$http', '$cookies', '$localStorage', '$location', '$rootScope', '$anchorScroll', '$timeout', '$window', '$stateParams', '$filter', '$interval', 'uploadFile', '$cookieStore', '$mdDialog', 'error'];
			function DialogController1($scope, $http, $cookies, $localStorage, $location, $rootScope, $anchorScroll, $timeout, $window, $stateParams, $filter, $interval, uploadFile, $cookieStore, $mdDialog, error) {

				if($location.host() == 'localhost' || $location.host() == '127.0.0.1' ){
					$scope.host = 'http://localhost:82';
				}else{
					$scope.host = 'http://35.231.238.182:82';
				}    

				$scope.error = error;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};

				$scope.cancel = function() {
					$mdDialog.cancel();
				};

				$scope.answer = function(answer) {
					$mdDialog.hide(answer);
				};

				$scope.saveTeam = function(captain, viceCaptain, teamName){

					var teamPlayerList = '';

					if(captain == viceCaptain){
						alert('Captain and Vice Captain can not be the same');
					}else if(parseInt($cookies.get('playerCount')) != 11){
						alert('Select exact 11 Players');
					}else{

						var objPlayerList    = [];
						var sessionKey       = $cookies.get('sessionKey');
						var matchId          = $cookies.get('matchId');
						var matchKey         = $cookies.get('matchKey');
						var apikey           = $cookies.get('apikey'); 
						var id               = $cookies.get('id'); 
						objPlayerList        = $cookieStore.get('listPlayer');
						var userTeam        = [];
			
						for (var i = 0; i < objPlayerList.length; i++) {
							teamPlayerList  += objPlayerList[i].id;
							if(i < 10){
								teamPlayerList 	+='#'; 	
							}
						}

						//FIRE API CALL TO LARAVEL SYSTEM FOR SAVING THE TEAM
						$http({
							method: 'POST',
							headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
							transformRequest: function(obj) {
								var str = [];
								for(var p in obj)
								str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data:{sessionKey: sessionKey, matchId: matchId, matchKey: matchKey, apikey: apikey, id: id, teamPlayerList: teamPlayerList, captain: captain, viceCaptain: viceCaptain, teamName: teamName},
							url: $scope.host+'/api/save-users-team'
						}).then(function successCallback(response) {
							if(response.data.code == '200'){
								$cookies.put('teamId', response.data.userTeam[0].teamId);
								$scope.userCurrentTeam = response.data.userTeam[0];
								$scope.closeDialog(); 
								window.location = '/app/index.html?#/app/dashboard';  
							}
						}, function errorCallback(data) {
							 
						});
					}
				
				}


			} //END OF DialogController1


})(window.angular); //END OF MAIN FUNCTION