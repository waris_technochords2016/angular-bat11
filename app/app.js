/**
 * @ngdoc overview
 * @name bat11App
 * @description
 * # bat11App
 *
 * Main module of the application.
 */
(function() {
    'use strict';
    angular
      .module('bat11App', [    
        'ngAnimate',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ngStorage',
        'ngCookies',
        'ngStore',
        'ngRoute',
        'ui.router',
        'ui.utils',
        'ui.load',
        'ui.jp',
        // 'oc.lazyLoad',
        'ui.bootstrap',
        'ui.date',
        'rzModule',
        'ngMaterial',
        'ngMessages', 
        'material.svgAssetsCache',
        'ui.carousel',
        'angular-page-loader',
      ]);
      
})();