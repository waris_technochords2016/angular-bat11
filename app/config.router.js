/**
* @ngdoc function
* @name app.config:uiRouter
* @description
* # Config
* Config for the router
*/
(function () {
    'use strict';
        angular
            .module('bat11App')
        	.run(runBlock)
            .config(config);

            runBlock.$inject = ['$rootScope', '$state', '$stateParams', '$timeout'];
			function runBlock($rootScope, $state, $stateParams, $timeout) {
				$rootScope.$state = $state;
				$rootScope.$stateParams = $stateParams;

				// Use a root scope flag to access everywhere in your app
  				$rootScope.isLoading = true;					 
			    $timeout(function() { // simulate long page loading 
			        $rootScope.isLoading = false; // turn "off" the flag 
			    }, 3000);
			}

            config.$inject = ['$stateProvider', '$urlRouterProvider'];
            function config($stateProvider, $urlRouterProvider) {

            	//LOAD TEMPLATE
            	var p = getParams('layout'),
		            l = p ? p + '.' : '',
            		layout = '/app/views/layout/layout.html';

            	$urlRouterProvider
        			.otherwise('/app/home');
            	
        		//Home Directory	
                $stateProvider
                	.state('bat11App', {
			            abstract: true,
			            url: '/app',
			            views: {
			                '': {
			                    templateUrl: layout
			                }
			            }
			        })

            		.state('bat11App.home', {
	                   url: '/home',
	                   templateUrl: '/app/views/home/home.html',
	                   data: { title: 'Welcome', hideFooter: true },
	               	})

                	//About Directory
            		.state('bat11App.about', {
				       url: '/about',
				       templateUrl: '/app/views/about/about.html',
				       data: { title: 'About Us', hideFooter: true },
				   	})

	               	.state('bat11App.legality', {
	                   url: '/legality',
	                   templateUrl: '/app/views/about/legality.html',
	                   data: { title: 'Current Regulatory Framework', hideFooter: true },
	               	})

	               	.state('bat11App.fantasyCricketContest', {
	                   url: '/fantasyCricketContest',
	                   templateUrl: '/app/views/about/fantasyCricketContest.html',
	                   data: { title: '   Fantasy Cricket Contests', hideFooter: true },
	               	})

	               	.state('bat11App.faq', {
	                   url: '/faq',
	                   templateUrl: '/app/views/about/faq.html',
	                   data: { title: 'FAQ', hideFooter: true },
	               	})

	               	.state('bat11App.howToPlay', {
	                   url: '/howToPlay',
	                   templateUrl: '/app/views/about/howToPlay.html',
	                   data: { title: ' How to Play Fantasy Cricket', hideFooter: true },
	               	})

	               	.state('bat11App.privacyPolicy', {
	                   url: '/privacyPolicy',
	                   templateUrl: '/app/views/about/privacyPolicy.html',
	                   data: { title: 'Privacy Policy', hideFooter: true },
	               	})

	               	.state('bat11App.siteMap', {
	                   url: '/siteMap',
	                   templateUrl: '/app/views/about/siteMap.html',
	                   data: { title: 'Site Map', hideFooter: true },
	               	})

	               	.state('bat11App.termsConditions', {
	                   url: '/termsConditions',
	                   templateUrl: '/app/views/about/termsConditions.html',
	                   data: { title: 'Terms and Conditions', hideFooter: true },
	               	})
	               	
	               	//BLOG Directory
	               	.state('bat11App.blog', {
	                   url: '/blog',
	                   templateUrl: '/app/views/blog/blog.html',
	                   data: { title: 'Blog', hideFooter: true },
	               	})
					
					.state('bat11App.blogReader', {
	                   url: '/blogReader',
	                   templateUrl: '/app/views/blog/blogReader.html',
	                   data: { title: 'Read Full Blog', hideFooter: true },
	               	})

	               	//NEWS Directory
					.state('bat11App.news', {
	                   url: '/news',
	                   templateUrl: '/app/views/news/news.html',
	                   data: { title: 'news', hideFooter: true },
	               	})
					
					.state('bat11App.newsReader', {
	                   url: '/newsReader',
	                   templateUrl: '/app/views/news/newsReader.html',
	                   data: { title: 'Read Full News', hideFooter: true },
	               	})

				 	//Contact Directory
	               	.state('bat11App.contact', {
	                   url: '/contact',
	                   templateUrl: '/app/views/contact/contactus.html',
	                   data: { title: 'Contact Us', hideFooter: true },
	               	})

	               	.state('bat11App.job', {
	                   url: '/job',
	                   templateUrl: '/app/views/contact/job.html',
	                   data: { title: 'Jobs', hideFooter: true },
	               	})

               		.state('bat11App.joinLeagues', {
	                   url: '/joinLeagues',
	                   templateUrl: '/app/views/contact/joinLeagues.html',
	                   data: { title: 'Join Leagues', hideFooter: true },
	               	})
	               	
	               	.state('bat11App.payment', {
	                   url: '/payment',
	                   templateUrl: '/app/views/contact/payment.html',
	                   data: { title: 'Payments', hideFooter: true },
	               	})
			 		
	               	//Fixture Directory
	               	.state('bat11App.upcoming', {
	                   url: '/upcoming',
	                   templateUrl: '/app/views/fixture/upcomingMatches.html',
	                   data: { title: 'Upcoming Matches', hideFooter: true },
	               	})

	               	.state('bat11App.liveScore', {
	                   url: '/liveScore',
	                   templateUrl: '/app/views/fixture/cricketLiveScore.html',
	                   data: { title: ' Cricket Live Scores', hideFooter: true },
	               	})
	                
	                //Team - List Directory

	               	.state('bat11App.teams', {
	                   url: '/teams',
	                   templateUrl: '/app/views/team-list/team-list.html',
	                   data: { title: 'Teams', hideFooter: true },
	               	})

	               	.state('bat11App.teamDetails', {
	                   url: '/teamDetails',
	                   templateUrl: '/app/views/team-list/team-details.html',
	                   data: { title: 'Team Details', hideFooter: true },
	               	})

	                .state('bat11App.players', {
	                   url: '/players',
	                   templateUrl: '/app/views/team-list/player-list.html',
	                   data: { title: ' Players', hideFooter: true },
	               	})

	               	.state('bat11App.playerDetails', {
	                   url: '/playerDetails',
	                   templateUrl: '/app/views/team-list/player-details.html',
	                   data: { title: ' Player Details', hideFooter: true },
	               	})

	               	//GALLERY Directory
	               	.state('bat11App.photos', {
	                   url: '/photos',
	                   templateUrl: '/app/views/gallery/photos.html',
	                   data: { title: 'Photos', hideFooter: true },
	               	})
	 			  	
	 			  	.state('bat11App.videos', {
	                   url: '/videos',
	                   templateUrl: '/app/views/gallery/videos.html',
	                   data: { title: 'Videos', hideFooter: true },
	               	})

	               	//pointTable Directory
	               	.state('bat11App.pointSystem', {
	                   url: '/pointSystem',
	                   templateUrl: '/app/views/pointTable/pointSystem.html',
	                   data: { title: 'Point System', hideFooter: true },
	               	})
	               	
	               	//Results Directory
	              	.state('bat11App.matchResults', {
	                   url: '/results',
	                   templateUrl: '/app/views/results/cricketMatchResults.html',
	                   data: { title: 'Cricket Match Results', hideFooter: true },
	               	})
               	            	

					//404-Page Directory
	               	.state('bat11App.404', {
	                   url: '/404',
	                   templateUrl: '/app/views/404-Page/404.html',
	                   data: { title: 'Page not Found', hideFooter: true },
	               	})

	              	//userInformation Directory
	               	.state('bat11App.dashboard', {
	                   url: '/dashboard',
	                   templateUrl: '/app/views/userInformation/userDashboard.html',
	                   data: { title: 'Dashboard', hideFooter: true },
	               	})
	              	
	              	//userInformation Directory
	               	.state('bat11App.createTeam', {
	                   url: '/createTeam',
	                   templateUrl: '/app/views/userInformation/createTeam.html',
	                   data: { title: 'Create Team', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.userProfile', {
	                   url: '/userProfile',
	                   templateUrl: '/app/views/userInformation/userprofile.html',
	                   data: { title: 'User Profile', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.otpVerification', {
	                   url: '/otpVerification',
	                   templateUrl: '/app/views/userInformation/otpVerification.html',
	                   data: { title: 'OTP Verification', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.bankDetails', {
	                   url: '/bankDetails',
	                   templateUrl: '/app/views/userInformation/bankDetails.html',
	                   data: { title: 'Bank Details', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.inviteFriends', {
	                   url: '/inviteFriends',
	                   templateUrl: '/app/views/userInformation/inviteFriends.html',
	                   data: { title: 'Invite Friends', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.mobiledropdownmenu', {
	                   url: '/mobiledropdownmenu',
	                   templateUrl: '/app/views/404-page/mobile-drop-down-menu.html',
	                   data: { title: 'Mobile Drop Down Menu', hideFooter: true },
	               	})


	               	//userInformation Directory
	               	.state('bat11App.mycontest', {
	                   url: '/mycontest',
	                   templateUrl: '/app/views/userInformation/userContestDetails.html',
	                   data: { title: 'My Contest Matches', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.matchcontestbets', {
	                   url: '/matchcontestbets',
	                   templateUrl: '/app/views/userInformation/userMatchContestBets.html',
	                   data: { title: 'My Contest', hideFooter: true },
	               	})

	               	//userInformation Directory
	               	.state('bat11App.mycontestteams', {
	                   url: '/mycontestteams',
	                   templateUrl: '/app/views/userInformation/userContestTeams.html',
	                   data: { title: 'My Contest Teams', hideFooter: true },
	               	})


	               	.state('bat11App.dinesh', {
	                   url: '/dinesh',
	                   templateUrl: '/app/views/404-Page/slider.html',
	                   data: { title: 'Test Slider' },
	               	})
		        ;  

				// function load(srcs, callback) {
				// 	return {
				// 	deps: ['$angularPageLoader', '$q',
				// 	  function ($angularPageLoader, $q) {
				// 	      var deferred = $q.defer();
				// 	      var promise = false;
				// 	      srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
				// 	      if (!promise) {
				// 	          promise = deferred.promise;
				// 	      }

				// 	      angular.forEach(srcs, function (src) {
				// 	          promise = promise.then(function () {
				// 	              angular.forEach(MODULE_CONFIG, function (module) {
				// 	                  if (module.name == src) {
				// 	                      src = module.module ? module.name : module.files;
				// 	                  }
				// 	              });
				// 	              return $angularPageLoader.load(src);
				// 	          });
				// 	      });


				// 	      deferred.resolve();
				// 	      return callback ? promise.then(function () { return callback(); }) : promise;
				// 	  }]
				// 	}
				// }


				// function load(srcs, callback) {
				// 	return {
				// 	deps: ['$ocLazyLoad', '$q',
				// 	  function ($ocLazyLoad, $q) {
				// 	      var deferred = $q.defer();
				// 	      var promise = false;
				// 	      srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
				// 	      if (!promise) {
				// 	          promise = deferred.promise;
				// 	      }
				// 	      angular.forEach(srcs, function (src) {
				// 	          promise = promise.then(function () {
				// 	              angular.forEach(MODULE_CONFIG, function (module) {
				// 	                  if (module.name == src) {
				// 	                      src = module.module ? module.name : module.files;
				// 	                  }
				// 	              });
				// 	              return $ocLazyLoad.load(src);
				// 	          });
				// 	      });

				// 	      deferred.resolve();
				// 	      return callback ? promise.then(function () { return callback(); }) : promise;
				// 	  }]
				// 	}
				// }


		        function getParams(name) {
		        	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	                results = regex.exec(location.search);
		            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		        }

            }

})();
