/**
 * @ngdoc function
 * @name bat11App.controller:USER AUTHENTICATION CONTROLLER
 * @description
 * # MainCtrl
 * Controller of the bat11App
 */

(function () {
    'use strict';
    angular
        .module('bat11App')
            
            .controller('userLogInCtrl', userLogInCtrl)
            .controller('userWithdrawCtrl',userWithdrawCtrl)

            .service('uploadFile', ['$http', function ($http) {
                this.uploadFiletoServer = function(file, uploadUrl){
                     var fd = new FormData();
                     fd.append('file', file);
                     $http.post(uploadUrl, fd, {
                         transformRequest: angular.identity,
                         headers: {'Content-Type': undefined,'Process-Data': false}
                     })
                     .success(function(data){
                        alert(data);
                     })
                     .error(function(){
                        alert("Error");
                     });
                }
            }]);

            userLogInCtrl.$inject = ['$scope', '$http', '$cookies', '$localStorage', '$location', '$rootScope', '$anchorScroll', '$timeout', '$window', '$stateParams', 'uploadFile', '$mdDialog' ];
            function userLogInCtrl($scope, $http, $cookies, $localStorage, $location, $rootScope, $anchorScroll, $timeout, $window, $stateParams, uploadFile, $mdDialog ) {
            
                if($location.host() == 'localhost' || $location.host() == '127.0.0.1' ){
                    $scope.host = 'http://localhost:82';
                }else{
                    $scope.host = 'http://35.231.238.182:82';
                }
               
                $scope.doLogin = function(user) {

                    var email      = user.email;
                    var password   = user.password;
                    
                    $('.doLoginError').html('');

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data:{email: email,password: password},
                        url: $scope.host+'/api/login'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){

                            //Hide the popup menu
                            $('#ModalLogin').addClass('hide');
                            $('#aside').modal('hide');
                            $('body').removeClass('modal-open').find('.modal-backdrop').remove();
                            $('.navbar-toggleable-sm').collapse('hide');
                   
                            // Set Apikey Into Session/Cookie
                            $cookies.put('id',response.data.objUser[0].id);
                            $cookies.put('firstname',response.data.objUser[0].firstname);
                            $cookies.put('lastname',response.data.objUser[0].lastname);
                            $cookies.put('email',response.data.objUser[0].email);
                            $cookies.put('teamname',response.data.objUser[0].teamname);
                            $cookies.put('phonenumber',response.data.objUser[0].phonenumber);
                            $cookies.put('gender',response.data.objUser[0].gender);
                            $cookies.put('country',response.data.objUser[0].country);
                            $cookies.put('state',response.data.objUser[0].state);
                            $cookies.put('dob',response.data.objUser[0].dob);
                            $cookies.put('apikey',response.data.objUser[0].apikey);                            
                            $cookies.put('middlename',response.data.objUser[0].middlename);
                            $cookies.put('address',response.data.objUser[0].address);
                            $cookies.put('city',response.data.objUser[0].city);
                            $cookies.put('pincode',response.data.objUser[0].pincode);
                            $cookies.put('verified',response.data.objUser[0].verified);
                            if (response.data.objUser[0].image != '') {
                                $cookies.put('image',response.data.objUser[0].image);
                            }

                            //TOGGLE MENU NAME IF USER IS LOGGED IN
                            $('.cd-log_reg ').addClass('hide');
                            $('.ifUserLoggedIn').removeClass('hide');
                            $('.ifUserLoggedIn > .ec-navigation-login > ul > li').addClass('');
                            $('.ifUserLoggedIn > .ec-navigation-login > ul > li > .userName > a').html($cookies.get('firstname')+' '+$cookies.get('lastname')+ '<i class="fa fa-chevron-down padding-top20"></i>');

                            $cookies.put('sessionKey', 'allseason');

                            //OPEN USER DASHBOARD                                
                            window.location = '/app/index.html?#/app/dashboard';
                             
                        }else if(response.data.code == '400'){

                            $cookies.put('email',response.data.email);
                            $cookies.put('mobile',response.data.phonenumber);
                            
                            $scope.email = response.data.email;
                            $scope.mobile = response.data.mobile;

                            $("#ModalOtpSumbit").modal("show");
                            $('#ModalLogin').addClass('hide');
                            $scope.alertMessage();
                        }else{
                            $('.doLoginError').html('Wrong Email or Password');
                            $('.doLoginError').removeClass('hide');
                            $scope.alertMessage();
                        }
                        
                    }, function errorCallback(response) {
                        console.log('Error in doLogin API');

                    });
                }; // USER LOGIN CONTROLLER ENDS

                // USER REGISTRATION CONTROLLER STARTS
                $scope.doRegister   = function(user) {
                    var currentDate = new Date();
                    var minDate     = new Date(currentDate.getFullYear() - 18, currentDate.getMonth(), currentDate.getDate());
                    var id          =   '';
                    var error       =   'true';

                    $('.doRegisterError').html('');

                    if(angular.isUndefined(user)){
                        error = 'Please fill value in the form';
                    }else if(angular.isUndefined(user.firstname)){
                        error = 'Enter First Name';
                    }else if(angular.isUndefined(user.lastname)){
                        error = 'Enter Last Name';
                    }else if(angular.isUndefined(user.email)){
                        error = 'Enter Email';
                    }else if(angular.isUndefined(user.mobile)){
                        error = 'Enter Mobile';
                    }else if(angular.isUndefined(user.password1)){
                        error = 'Enter Password';
                    }else if(angular.isUndefined(user.password2)){
                        error = 'Re-Enter Password';
                    }else if(angular.isUndefined(user.dob)){
                        error = 'Select Date of Birth';
                    }else if(minDate < user.dob){
                        error = 'Age can not be less than 18 years.';
                    }else if(angular.isUndefined(user.selectedGender)){
                        error = 'Select Gender';
                    }else if(angular.isUndefined(user.selectedCountry)){
                        error = 'Select Country';
                    }else if(angular.isUndefined(user.selectedState)){
                        error = 'Select State';
                    }else if(user.password1 != user.password2){
                        error = 'Enter Same Password in both field';
                    }

                    if(error != 'true'){
                        $('.doRegisterError').removeClass('hide');
                        $('.doRegisterError').html(error);
                    }else{

                        var firstname           =   user.firstname;
                        var lastname            =   user.lastname;
                        var email               =   user.email;
                        var password1           =   user.password1;
                        var mobile              =   user.mobile;
                        var selectedGender      =   user.selectedGender;
                        var selectedCountry     =   user.selectedCountry.id;
                        var selectedState       =   user.selectedState.id;
                        var dob                 =   user.dob.getFullYear() +'-'+ user.dob.getMonth() +'-'+ user.dob.getDate();

                        $http({
                            method: 'POST',
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                            transformRequest: function(obj) {
                                var str = [];
                                for(var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                return str.join("&");
                            },
                            data:{firstname : firstname, lastname : lastname, email : email, password : password1, mobile : mobile, selectedGender : selectedGender, selectedCountry : selectedCountry, selectedState : selectedState, dob : dob},
                            url: $scope.host+'/api/registration'
                        }).then(function successCallback(response) {
                            if(response.data.code == '200'){

                                $cookies.put('email',email);
                                $cookies.put('mobile',mobile);
                                $("#ModalOtpSumbit").modal("show");
                                $('#ModalRegister').addClass('hide');
 
                                // $('.doRegisterError').removeClass('hide');
                                // $('.doRegisterError').html('SUCCESS');

                                //  //Hide the popup menu
                                // $('#ModalRegister').addClass('hide');
                                // $('#aside').modal('hide');
                                // $('body').removeClass('modal-open').find('.modal-backdrop').remove();
                                // $('.navbar-toggleable-sm').collapse('hide');

                                // // Set Apikey Into Session/Cookie
                                // $cookies.put('id',response.data.objUser.id);
                                // $cookies.put('firstname',response.data.objUser.firstname);
                                // $cookies.put('lastname',response.data.objUser.lastname);
                                // $cookies.put('email',response.data.objUser.email);
                                // $cookies.put('phonenumber',response.data.objUser.phonenumber);
                                // $cookies.put('gender',response.data.objUser.gender);
                                // $cookies.put('country',response.data.objUser.country);
                                // $cookies.put('state',response.data.objUser.state);
                                // $cookies.put('dob',response.data.objUser.dob);
                                // $cookies.put('apikey',response.data.objUser.apikey);

                                // //TOGGLE THE MENU BAR
                                // $('.cd-log_reg').addClass('hide');
                                // $('.ifUserLoggedIn').removeClass('hide');
                                // $('.ifUserLoggedIn > .ec-navigation-login > ul > li').addClass('fa fa-chevron-down');
                                // $('.ifUserLoggedIn > .ec-navigation-login > ul > li > .userName').html($cookies.get('firstname')+' '+$cookies.get('lastname'));

                                // window.location = '/app/index.html?#/app/dashboard';
                             
                            }else{
                                $('.doRegisterError').removeClass('hide');    
                                $('.doRegisterError').html('Email already Exist');
                                $scope.alertMessage();
                            }

                        }, function errorCallback(response) {
                                $('.doRegisterError').removeClass('hide');
                                $('.doRegisterError').html('FAILED');
                                $scope.alertMessage();
                            
                        });
                    }

                }; // USER REGISTRATION CONTROLLER ENDS


                // USER UPDATE PROFILE CONTROLLER STARTS
                       
                $scope.doUpdateProfile   = function(email, firstname, middlename, lastname, teamname, phonenumber, myFile, dob, gender, address, country, state, city, pincode) {
                 
                    var email               = $cookies.get('email');
                    var id                  = $cookies.get('id');
                    var apikey              = $cookies.get('apikey');
                    var image               = $scope.src;
                    var file                = '';

                    var stateId             = state;
                    var countryId           = country;
                    $('.doUpdateProfileError').html('');


                    if(!angular.isUndefined($scope.files)){
                        $scope.myFile = $scope.files[0];
                        var file = $scope.myFile; 
                    }

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                       
                       
                        data: {email: email, firstname: firstname, middlename: middlename, lastname: lastname, teamname: teamname, phonenumber: phonenumber, dob: dob, gender: gender, address: address, country: countryId, state: stateId, city: city, pincode: pincode, email: email, id: id, image: image, file: file, apikey: apikey}, 
                        url: $scope.host+'/api/update-profile'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){

                            $cookies.put('firstname',response.data.objUser.firstname);
                            $cookies.put('middlename',response.data.objUser.middlename);
                            $cookies.put('lastname',response.data.objUser.lastname);
                            $cookies.put('teamname',response.data.objUser.teamname);
                            $cookies.put('phonenumber',response.data.objUser.phonenumber);
                            $cookies.put('dob',response.data.objUser.dob);
                            $cookies.put('gender',response.data.objUser.gender);
                            $cookies.put('address',response.data.objUser.address);
                            $cookies.put('country',response.data.objUser.country);
                            $cookies.put('state',response.data.objUser.state);
                            $cookies.put('city',response.data.objUser.city);
                            $cookies.put('pincode',response.data.objUser.pincode);
                            $cookies.put('apikey',response.data.objUser.apikey);
                            if (response.data.objUser.image != '') {
                                $cookies.put('image',response.data.objUser.image);
                            }
                            $('.doUpdateProfileError').html('Your profile has been updated succefully!');
                            $('.doUpdateProfileError').removeClass('hide');
                            $scope.alertMessage();
                            $scope.userProfile();
                            window.location = '/app/index.html?#/app/userProfile';  
                         
                        }else{

                            $('.doUpdateProfileError').html('Error in updation of profile');
                            $('.doUpdateProfileError').removeClass('hide');
                            $scope.alertMessage();
                        }

                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });
                }; // USER UPDATE PROFILE CONTROLLER ENDS


                // GET THE IMAGE AFTER UPLOADING
                $scope.uploadedFile = function(element) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $scope.$apply(function($scope) {
                            $scope.files = element.files;
                            $scope.src = event.target.result  
                        });
                    }
                
                    reader.readAsDataURL(element.files[0]);

                }// GET THE IMAGE AFTER UPLOADING END


                // USER CHANGE PASSWORD CONTROLLER STARTS
                $scope.dochangePassword   = function(user) {
                    var passwordOld     = user.passwordOld;
                    var passwordNew1    = user.passwordNew1;
                    var passwordNew2    = user.passwordNew2;
                    var email           = $cookies.get('email');
                    var apikey          = $cookies.get('apikey'); 
                    var id              = $cookies.get('id'); 


                    $('.errorchangepassword').html('');
                    $('.errorchangepassword1').html('');

                    if(passwordNew1 != passwordNew2){

                        $('.errorchangepassword').html('please enter same password in new password & confirm new password');
                        $('.errorchangepassword').removeClass('hide');
                        $('.errorchangepassword1').html('please enter same password in new password & confirm new password');
                        $('.errorchangepassword1').removeClass('hide');
                        $scope.alertMessage();

                    }else if(passwordOld == '' || passwordNew1 == '' || passwordNew2 == ''){

                        $('.errorchangepassword').html('Field can\'t be empty');
                        $('.errorchangepassword').removeClass('hide');
                        $('.errorchangepassword1').html('Field can\'t be empty');
                        $('.errorchangepassword1').removeClass('hide');
                        $scope.alertMessage();

                    }else{
                        $http({
                            method: 'POST',
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                            transformRequest: function(obj) {
                                var str = [];
                                for(var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                return str.join("&");
                            },
                            data : {passwordOld : passwordOld, passwordNew1 : passwordNew1, passwordNew2 : passwordNew2, email: email, id: id, apikey: apikey},
                            url: $scope.host+'/api/changePassword'
                        }).then(function successCallback(response) {
                            if(response.data.code == '200'){
                                $('.errorchangepassword').html('Password Updated succefully');
                                $('.errorchangepassword').removeClass('hide');
                                $('.errorchangepassword1').html('Password Updated succefully');
                                $('.errorchangepassword1').removeClass('hide');
                                $scope.alertMessage();
                                window.location() = '/app/index.html?#/app/dashboard';
                                $("#errorMessageShow").modal("show");
                                $("#ModelChangePassword").addClass("hide");
                            }else{
                                $('.errorchangepassword').html('Error in updation of password');
                                $('.errorchangepassword').removeClass('hide');
                                $('.errorchangepassword1').html('Error in updation of password');
                                $('.errorchangepassword1').removeClass('hide');
                                $scope.alertMessage();
                            }

                        }, function errorCallback(response) {
                            console.log("Error in calling API");
                        });
                    }
                }; // USER CHANGE PASSWORD CONTROLLER ENDS

                 // USER FORGOT PASSWORD CONTROLLER STARTS
                $scope.doForgotPassword   = function(user) {

                    var email     = user.email;
                    $('.doForgotPasswordError').html('');

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data : {email : email},
                        url: $scope.host+'/api/forget-password'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $('.doForgotPasswordError').removeClass('hide');
                            $('.doForgotPasswordError').html(response.data.message);
                            $("#ModalLogin").modal("show");
                            $('#ModelForgotPassword').addClass('hide');
                            $scope.alertMessage();
                        }else{
                        }

                    }, function errorCallback(response) {
                        $scope.MessageData = JSON.parse(response);
                        console.log($scope.MessageData);
                    });
                    
                    // window.location = '/app/index.html?#/app/home';

                }; // USER FORGOT PASSWORD CONTROLLER ENDS


                //SENDING MOBILE NUMBER FOR OTP FUNCTION STARTS
                $scope.sendOTP  = function(mobile, email, flag){
                    $scope.mobile  = mobile;
                    $scope.email   = email;
                    $('.sendOTPerror').html('');

                     $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data : {phonenumber : mobile, email : email},
                        url: $scope.host+'/api/send-verified-code'
                       
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $cookies.put('mobile',response.data.phonenumber);
                            $cookies.put('email',response.data.email);
                            
                            $('.sendOTPerror').removeClass('hide');
                            $('.sendOTPerror').html(response.data.message);
                            // $('input[name=mobile]').val(response.data.phonenumber);
                            // $('input[name=email]').val(response.data.email);
                            $("#ModalOtpSumbit").modal("show");
                            $('#ModalOtpSend').addClass('hide');
                            $scope.alertMessage();
                        }else{

                            $('.sendOTPerror').html('Error in sending OTP');
                            $('.sendOTPerror').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(data) {
                    });

                }//SENDING MOBILE MOBILE NUMBER FOR OTP FUNCTION ENDS 


                //SUBMIT OTP FUNCTION STARTS
                $scope.submitOTP = function(otp){
                    var otp     = otp;
                    var mobile  = $cookies.get('mobile');
                    var email   = $cookies.get('email');

                    $('.submitOTPerror').html('');
           
                     $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },

                        data : {otpcode : otp, phonenumber : mobile, email : email},
                        url: $scope.host+'/api/otp-verification'
                       
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $('.submitOTPerror').removeClass('hide');
                            $('.submitOTPerror').html(response.data.message);        
                            $("#ModalLogin").modal("show");
                            $("#ModalOtpSumbit").addClass("hide");
                            $scope.alertMessage();
   
                        }else{

                            $('.submitOTPerror').removeClass('hide');
                            $('.submitOTPerror').html(response.data.message);        
                            $("#ModalOtpSend").modal("show");
                            $("#ModalOtpSumbit").addClass("hide");
                            $scope.alertMessage();

                        }
                    }, function errorCallback(data) {
                    });

                }//SUBMIT OTP FUNCTION ENDS 


                // USER BANK DETAILS UPDATE
                $scope.doUpdateBankDetails   = function(bankname, branch, holdername, accountnumber, ifsccode) {
                    var id                  = $cookies.get('id');
                    var email               = $cookies.get('email');
                    var apikey              = $cookies.get('apikey');
                    $('.bankerror').html('');
                   
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {bankname: bankname, branch: branch, holdername: holdername, accountnumber: accountnumber, ifsccode: ifsccode, email: email, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/update-bankdetails'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $cookies.put('bankname',response.data.objBankDetails.bankname);
                            $cookies.put('branch',response.data.objBankDetails.branch);
                            $cookies.put('holdername',response.data.objBankDetails.holdername);
                            $cookies.put('accountnumber',response.data.objBankDetails.accountnumber);
                            $cookies.put('ifsccode',response.data.objBankDetails.ifsccode);

                            $('.bankerror').html('Your bank details have been updated successfully');
                            $('.bankerror').removeClass('hide');
                            $scope.alertMessage();
                            $scope.userProfile();
                            //window.location() = '/app/index.html?#/app/bankDetails';
                        }else{
                            $('.bankerror').html('Error in updation of bank details');
                            $('.bankerror').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });
                }; // USER BANK DETAILS UPDATE


                // SEND INVITATION DETAILS
                $scope.sendInvitation = function(referemail, message) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');
                    var link          = $scope.host+'/app/index.html?#/app/userProfile';

                    $('.errorinvitation').html('');
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {referemail: referemail, message: message, link: link, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/send-invitation-card'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $cookies.put('referemail',response.data.objSendInvitaion.referemail);
                            $cookies.put('message',response.data.objSendInvitaion.message);
                            $cookies.put('link',response.data.objSendInvitaion.link);

                            $('.errorinvitation').html('Your Invitation has been send successfully!');
                            $('.errorinvitation').removeClass('hide');
                            $scope.alertMessage();
                            $scope.userProfile();
                            //window.location() = '/app/index.html?#/app/inviteFriends';
                        }else{
                            $('.errorinvitation').html('Error in sending of invitation');
                            $('.errorinvitation').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });
                }; // USER BANK DETAILS UPDATE


                // SEND INVITATION DETAILS
                $scope.acceptInvitation = function(referalcode) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');

                    $('.erroracceptinvitation').html('');
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {referalcode: referalcode, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/accept-invitation'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $('.referalcode').val('');
                            $('.erroracceptinvitation').html('Invitation has been accepted successfully!');
                            $('.erroracceptinvitation').removeClass('hide');
                            $scope.alertMessage();
                            $scope.userProfile();
                            //window.location() = '/app/index.html?#/app/inviteFriends';
                        }else{
                            $('.erroracceptinvitation').html('Error in accepting of invitation');
                            $('.erroracceptinvitation').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });
                }; // USER BANK DETAILS UPDATE

                //DELETE SEND INVITATION DETAILS
                $scope.deleteSendInvitation = function(inviteid) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');

                    $('.errordeletesendinvitation').html('');
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {inviteid: inviteid, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/delete-send-invitation'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $('.errordeletesendinvitation').html('Thank you.Your Invitation has been deleted!');
                            $('.errordeletesendinvitation').removeClass('hide');
                            $scope.alertMessage();
                            $scope.userProfile();
                            //window.location() = '/app/index.html?#/app/inviteFriends';
                        }else{
                            $('.errordeletesendinvitation').html('Error in delete of invitation');
                            $('.errordeletesendinvitation').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });
                }; // DELETE SEND INVITATION DETAILS

                //DELETE ACCEPT INVITATION DETAILS
                $scope.deleteAcceptInvitation = function(inviteid) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');

                    $('.errordeleteacceptinvitation').html('');
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {inviteid: inviteid, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/delete-accept-invitation'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $('.errordeleteacceptinvitation').html('Thank you.Your Invitation has been deleted!');
                            $('.errordeleteacceptinvitation').removeClass('hide');
                            $scope.alertMessage();
                            $scope.userProfile();
                            //window.location() = '/app/index.html?#/app/inviteFriends';
                        }else{
                            $('.errordeleteacceptinvitation').html('Error in delete of invitation');
                            $('.errordeleteacceptinvitation').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });
                }; // DELETE ACCEPT INVITATION DETAILS

                $scope.alertMessage = function(){
                    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                        $(".alert").slideUp(500);
                    });
                }

                
              
            }

            function userWithdrawCtrl($scope,$mdDialog, $cookies, $http, $location, uploadFile ){

                $scope.status = '  ';
                $scope.customFullscreen = false;
                if($location.host() == 'localhost' || $location.host() == '127.0.0.1' ){
                    $scope.host = 'http://localhost:82';
                }else{
 
                    $scope.host = 'http://35.231.238.182:82';
 
                }    

                $scope.showPatient = function(ev) {
                // Appending dialog to document.body to cover sidenav in docs app
                    var confirm = $mdDialog.confirm()
                        .title('Would you like to deposit amount?')
                        .textContent('Please give us some time as your verification is under process..!!')
                        .ariaLabel('Your Deposit')
                        .targetEvent(ev)
                        .ok('Please wait!')
                        .cancel('Closed');

                    $mdDialog.show(confirm).then(function() {
                      $scope.status = 'Please give us some time as your verification is under process..!!';
                    }, function() {
                      $scope.status = 'You closed the dialog.';
                    });
                };

                $scope.showWithdraw = function(ev) {
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'withdraw.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                    .then(function(answer) {
                      $scope.status = answer;
                    }, function() {
                      $scope.status = 'You cancelled the dialog.';
                    });
                };


                $scope.showAddFunds = function(ev) {
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'addfund.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                    .then(function(answer) {
                      $scope.status = answer;
                    }, function() {
                      $scope.status = 'You cancelled the dialog.';
                    });
                };

                $scope.showVarifyBox = function(ev) {
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'uploaddocument.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose:true
                    })
                    .then(function(answer) {
                      $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                      $scope.status = 'You cancelled the dialog.';
                    });
                };

                $scope.showTransactionDetails = function(ev, transId) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');
                    // $scope.objTranscationDetails    = [];

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {transId: transId, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/transaction-details'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objTranscationDetails    = [];
                            $scope.objTranscationDetails = response.data.getTransactionDetailObj;
                            console.log($scope.objTranscationDetails);

                             $mdDialog.show({
                              targetEvent: event,
                              templateUrl: 'showtransactiondetails.tmpl',
                              controller: 'DialogController1',
                              clickOutsideToClose:true,
                              // onComplete: $scope.afterShowAnimation,
                              locals: { error : $scope.objTranscationDetails }
                           });
             
                        }else{
                            
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });

                };

                $scope.sendTransactionEmail = function(ev, transId) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');
                    // $scope.objTranscationDetails    = [];

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {transId: transId, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/send-transaction-email'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objTranscationDetails    = [];
                            $scope.objTranscationDetails = response.data.getTransactionDetailObj;
                            console.log($scope.objTranscationDetails.email);

                             $mdDialog.show({
                              targetEvent: event,
                              templateUrl: 'sendTransactionEmail.tmpl',
                              controller: 'DialogController1',
                              clickOutsideToClose:true,
                              // onComplete: $scope.afterShowAnimation,
                              locals: { error : $scope.objTranscationDetails.email }
                           });
             
                        }else{
                            
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });

                };


                $scope.downloadTransactionInvoice = function(ev, transId) {
                    var id            = $cookies.get('id');
                    var apikey        = $cookies.get('apikey');
                    // $scope.objTranscationDetails    = [];

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {transId: transId, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/download-transaction-email'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objTranscationInvoicePdf    = [];
                            $scope.objTranscationInvoicePdf = response.data.downloadTransactionPdfObj;
                            console.log($scope.objTranscationInvoicePdf);

                             $mdDialog.show({
                              targetEvent: event,
                              templateUrl: 'downloadTransactionInvoice.tmpl',
                              controller: 'DialogController1',
                              clickOutsideToClose:true,
                              // onComplete: $scope.afterShowAnimation,
                              locals: { error : $scope.objTranscationInvoicePdf }
                           });
             
                        }else{
                            
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                    });

                };

             
            function DialogController($scope, $mdDialog, $cookies, $http , $location, uploadFile) {
                 if($location.host() == 'localhost' || $location.host() == '127.0.0.1' ){
                    $scope.host = 'http://localhost:82';
                }else{
 
                    $scope.host = 'http://35.231.238.182:82';
 
                }    

                $scope.hide = function() {
                  $mdDialog.hide();

                };

                $scope.cancel = function() {
                  $mdDialog.cancel();
                };

                $scope.answer = function(answer) {
                    $mdDialog.hide(answer);
                };

                $scope.withdrawAmount   = function(fund) {
                    var id                  = $cookies.get('id');
                    var email               = $cookies.get('email');
                    var apikey              = $cookies.get('apikey');
                    $('.withdraw-error').html('');
                           
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {fund: fund, email: email, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/user-withdraw-funds'
                    }).then(function successCallback(response) {
                        
                        if(response.data.code == '200'){
                            $scope.objUserFund = response.data.objBankDetails.fund;
                            $('.userFund').html('<i class="fa fa-rupee"></i> '+$scope.objUserFund);
                            $scope.answer('Your '+fund+' has been debited to your account.'); 
                            //window.location() = '/app/index.html?#/app/bankDetails';
                        }else{
                            $scope.answer('Your can not debited amount to your account.'); 
                            $('.withdraw-error').html('Error in updation of bank details');
                            $('.withdraw-error').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                        
                    });
                }; 


                $scope.userFundAmount   = function(fund) {
                    var id                  = $cookies.get('id');
                    var email               = $cookies.get('email');
                    var apikey              = $cookies.get('apikey');
                    $('.withdraw-error').html('');
                           
                    // $scope.withdrawDetails();

                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {fund: fund, email: email, id: id, apikey: apikey}, 
                        url: $scope.host+'/api/user-add-funds'
                    }).then(function successCallback(response) {
                        if(response.data.code == '200'){
                            $scope.objUserFund = response.data.objBankDetails.fund;
                            $('.userFund').html('<i class="fa fa-rupee"></i> '+$scope.objUserFund);
                            $scope.answer('Your '+fund+' has been cedited to your account.'); 

                            //window.location() = '/app/index.html?#/app/bankDetails';
                        }else{
                            $('.withdraw-error').html('Error in updation of bank details');
                            $('.withdraw-error').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                        
                    });
                }; 

                $scope.userVefiryDocument   = function(pancardno, pancardImageFile, addressproof, addressProofFile, mobile, bankname, branch, holdername, accountnumber, ifsccode) {

                    var id                  = $cookies.get('id');
                    var email               = $cookies.get('email');
                    var apikey              = $cookies.get('apikey');
                   
                    var pancardimage        = $scope.srcpancard;
                    var pancardfile        = '';

                    if(!angular.isUndefined($scope.pancardfiles)){
                        $scope.pancardImageFile = $scope.pancardfiles[0];
                        var pancardfile = $scope.pancardImageFile; 
                    }

                    var addressimage        = $scope.srcaddressimage;
                    var addressfile        = '';

                    if(!angular.isUndefined($scope.addressfiles)){
                        $scope.addressProofFile = $scope.addressfiles[0];
                        var addressfile = $scope.addressProofFile; 
                    }
                    

                    $('.withdraw-error').html('');
                           
                    $http({
                        method: 'POST',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        transformRequest: function(obj) {
                            var str = [];
                            for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {bankname: bankname, branch: branch, holdername: holdername, accountnumber: accountnumber, ifsccode: ifsccode, pancardno: pancardno, pancardimage: pancardimage, pancardfile: pancardfile, addressimage: addressimage, addressfile: addressfile, addressproof: addressproof,  email: email, id: id, apikey: apikey, mobile: mobile}, 
                        url: $scope.host+'/api/user-update-documents'
                    }).then(function successCallback(response) {
                        
                        if(response.data.code == '200'){
                            $scope.answer('Your documant has been updated to your account.');
                            $scope.withdrawDetails(); 
                            //window.location() = '/app/index.html?#/app/bankDetails';
                        }else{
                            $('.withdraw-error').html('Error in updation of bank details');
                            $('.withdraw-error').removeClass('hide');
                            $scope.alertMessage();
                        }
                    }, function errorCallback(response) {
                       console.log('Error in Calling API');
                        
                    });
                }; 

                // GET THE IMAGE AFTER UPLOADING
                $scope.uploadedPancardFile = function(element) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $scope.$apply(function($scope) {
                            $scope.pancardfiles = element.files;
                            $scope.srcpancard = event.target.result  
                        });
                    }
                
                    reader.readAsDataURL(element.files[0]);

                }// GET THE IMAGE AFTER UPLOADING END

                // GET THE IMAGE AFTER UPLOADING
                $scope.uploadedAddressFile = function(element) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $scope.$apply(function($scope) {
                            $scope.addressfiles = element.files;
                            $scope.srcaddressimage = event.target.result  
                        });
                    }
                
                    reader.readAsDataURL(element.files[0]);

                }// GET THE IMAGE AFTER UPLOADING END

            }
        }

            
})(window.angular);