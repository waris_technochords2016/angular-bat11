/*global $:false */

jQuery(document).ready(function(){'use strict';
	
	/* -------------------------------------- */
	// 		RTL Support Visual Composer
	/* -------------------------------------- */	
	jQuery(window).on('load',function(){'use strict';

		jQuery('#flexCarousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: true,
            itemWidth: 70,
            asNavFor: '#postSlider'
        });

        jQuery('#postSlider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: true,
            sync: "#flexCarousel"
        });
    });


	jQuery('#gallerycarousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 75,
		itemMargin: 27,
		asNavFor: '#galleryslider'
	});

	jQuery('#galleryslider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#gallerycarousel"
	});
});